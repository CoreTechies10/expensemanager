<?php
/**
 * Created by PhpStorm.
 * User: hitesh
 * Date: 9-10-15
 * Time: 13:35
 */

function all_list_handler(){
  $users = entity_load('user');

  $header = array(
    'id' => t('Id'),
    'c_name' => t('Company Name'),
    'f_name' => t('First name'),
    'type' => t('Type(Premium)'),
    'details' => t('Details'),
  );
  $rows = array();
  foreach ($users as $user ) {
    if($user->status == 1 && is_array($user->data) && $user->data['company']) {
      $rows[] = array(
        'id' => $user->uid,
        'c_name' => $user->data['company'],
        'f_name' => $user->data['first_name'],
        'type' => isset($user->data['premium']) ? ($user->data['premium'] ? t('Yes') : t('No')) : t('No'),
        'details' => "<a href='/admin/expense_manager/manage_company/{$user->uid}' title='Get details of user {$user->name}'>View</a>",
      );
    }
  }


  return theme('table', array('header' => $header, 'rows' => $rows));
}

function global_settings(){
  return __FUNCTION__;
}

function manage_company($id=null){
  return __FUNCTION__;
}