<?php
/**
 * Created by PhpStorm.
 * User: hitesh
 * Date: 17-9-15
 * Time: 17:33
 */

/**
 * This files will handles all menu system
 * which by the way generated dynamically
 */

/*
 * This function is responsible for provide menu in admin panel
 */
function main_admin_panel_menu(&$menus) {

  //Default menu
  $menus['admin/expense_manager/list'] = array(
    'title' => 'List of company',
    'description' => 'Complete list of registered companies',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -8
  );
  $menus['admin/expense_manager'] = array( # will found in admin
    'title' => 'Expense Manager',
    'description' => 'Manage things',
    'page callback' => 'all_list_handler',
    'page arguments' => array(),
    'file path' => drupal_get_path('module', 'petty_cash') . '/includes/',
    'file' => 'admin.inc.php',
    'access arguments' => array('administer users'),
    'position' => 'left',
    'weight' => 0,

  );
  //End

  //Tab Global Setting
  $menus['admin/expense_manager/global_settings'] = array(
    'title' => 'Global Setting',
    'description' => 'Set Global setting for users module',
    'access arguments' => array('administer user'),
    'page callback' => 'global_settings',
    'type' => MENU_LOCAL_TASK,
    'weight' => -2,
    'file path' => drupal_get_path('module', 'petty_cash') . '/includes/',
    'file' => 'admin.inc.php',
  );




  /***************************/
  $menus['admin/expense_manager/manage_company/%id'] = array(
    'title' => 'Add Subject',
    'page callback' => array('manage_company'),
    'page arguments' => array(1),
    'access arguments' => array('administer users'),
    'file path' => drupal_get_path('module', 'petty_cash') . '/includes/',
    'file' => 'admin.inc.php',
    'type' => MENU_LOCAL_TASK,
  );


  /**************************
  //Making available in admin navigation bar
  $menus[ 'expense_manager' ] = array(
    'title' => 'Manage Expense',
    //'title callback' => t(), #May handle Dynamic title for defer users
    'description' => t('Here you can manage Upcoming events, meetings etc.'),
    'page callback' => 'event_manager',
    //'page arguments' => array('event_manager'),
    'file path' => drupal_get_path('module', 'petty_cash') . '/includes/',
    'file' => 'event.inc.php',
    'access arguments' => array('administer users'),
  );

  $menus[ 'expense_manager/' ] = array(
    'title' => 'Create Event',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10
  );*/

  /***************************/

  /**
   * Will show the list of all meeting/meeting types
   */
  $menus[ 'expense_manager/meeting' ] = array(
    'title' => 'Meeting',
    'page callback' => 'meeting_index',
    'file' => 'meeting.page.php',
    'file path' => drupal_get_path('module', 'petty_cash') . '/pages/',
    'access callback' => TRUE,
    'type' => MENU_LOCAL_TASK,
  );

  $menus[ 'expense_manager/meeting/' ] = array(
    'title' => 'Meeting',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10
  );

  /**
   * Created new supplier list
   */
  $menus[ 'expense_manager/meeting/create' ] = array(
    'title' => 'Create',
    'description' => t('Here you can create new supplier'),
    'page callback' => 'create_meeting',
    'file' => 'meeting.page.php',
    'file path' => drupal_get_path('module', 'petty_cash') . '/pages/',
    'access callback' => TRUE,
    'type' => MENU_LOCAL_TASK,
  );

  /**
   * Created new supplier list
   */
  $menus[ 'expense_manager/meeting_type/create' ] = array(
    'title' => 'Create',
    'description' => t('Here you can create new supplier'),
    'page callback' => 'create_meeting_type',
    'file' => 'meeting.page.php',
    'file path' => drupal_get_path('module', 'petty_cash') . '/pages/',
    'access callback' => TRUE,
    'type' => MENU_LOCAL_TASK,
  );

  /**
   * Update new supplier list/ Delete
   */
  $menus[ 'expense_manager/meeting/update/' ] = array(
    'title' => 'Update',
    'description' => t('Here you can update new supplier'),
    'page callback' => 'update_meeting',
    'page arguments' => array(''),
    'file' => 'meeting.page.php',
    'file path' => drupal_get_path('module', 'petty_cash') . '/pages/',
    'access callback' => TRUE,
    'type' => MENU_LOCAL_TASK,
  );



  /***************************/

  /**
   * Will show the list of all supplier
   */
  $menus[ 'expense_manager/supplier' ] = array(
    'title' => 'Supplier',
    'page callback' => 'supplier_list',
    'file' => 'supplier.page.php',
    'file path' => drupal_get_path('module', 'petty_cash') . '/pages/',
    'access callback' => TRUE,
    'type' => MENU_LOCAL_TASK,
  );

  $menus[ 'expense_manager/supplier/' ] = array(
    'title' => 'Supplier',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10
  );

  /**
   * Created new supplier list
   */
  $menus[ 'expense_manager/supplier/create' ] = array(
    'title' => 'Create',
    'description' => t('Here you can create new supplier'),
    'page callback' => 'create_supplier',
    'file' => 'supplier.page.php',
    'file path' => drupal_get_path('module', 'petty_cash') . '/pages/',
    'access callback' => TRUE,
    'type' => MENU_LOCAL_TASK,
  );

  /**
   * Update new supplier list/ Delete
   */
  $menus[ 'expense_manager/supplier/update/' ] = array(
    'title' => 'Update',
    'description' => t('Here you can update new supplier'),
    'page callback' => 'update_supplier',
    'page arguments' => array(''),
    'file' => 'event.page.php',
    'file path' => drupal_get_path('module', 'petty_cash') . '/pages/',
    'access callback' => TRUE,
    'type' => MENU_LOCAL_TASK,
  );

  /****************************/



  /***************************/
  $menus[ 'expense_manager/add_edit' ] = array(
    'title' => 'Add Edit',
    'page callback' => 'add_edit',
    'file' => 'others.page.php',
    'file path' => drupal_get_path('module', 'petty_cash') . '/pages/',
    'access callback' => TRUE,
    'type' => MENU_LOCAL_TASK,
  );

  /***************************/
}
