<?php
/**
 * Created by PhpStorm.
 * User: hitesh
 * Date: 25-9-15
 * Time: 17:50
 */

function meeting_index() {
  global $user;
  $meetings = db_select('meeting', 'm');
  $meetings->join('meeting_type', 'mt', 'm.MEETING_TYPE_fk = mt.ID');
  $meetings->join('location', 'l', 'm.LOCATION_fk = l.ID');
  $meetings->fields('m')
    ->fields('mt', array('name'))
    ->fields('l', array('name'));
  $meetings = $meetings->condition('m.UID_fk', $user->uid)
    ->execute()
    ->fetchAllAssoc('ID', PDO::FETCH_ASSOC);

  return theme('meeting', array('meetings' => $meetings));
}

function create_meeting() {
  $fields = array(
    'type' => '',
    'location' => '',
    'form' => array(),
    'supplier' => array(),
  );
  global $user;
  $fields['type'] = db_select('meeting_type', 't')
    ->fields('t')
    ->where('UID_fk = :uid', array(':uid' => $user->uid))
    ->execute()
    ->fetchAllAssoc('ID', PDO::FETCH_ASSOC);
  $fields['location'] = db_select('location', 't')
    ->fields('t')
    ->where('UID_fk = :uid', array(':uid' => $user->uid))
    ->execute()
    ->fetchAllAssoc('ID', PDO::FETCH_ASSOC);
  $fields['supplier'] = db_select('supplier', 't')
    ->fields('t')
    ->where('UID_fk = :uid', array(':uid' => $user->uid))
    ->execute()
    ->fetchAllAssoc('ID', PDO::FETCH_ASSOC);


  $fields['form'] = drupal_get_form('form_meeting_create', array('type' => $fields['type'], 'location' => $fields['location'], 'supplier' => $fields['supplier']));

  return theme('meeting_create', array('form' => $fields['form']));
}

/********************/


/**
 * @param $form
 * @param $form_state
 * @return mixed
 * @see meeting_index
 */
function form_meeting_create($form, &$form_state) {

  $args = $form_state['build_info']['args'][0];

  //list of location options
  $type_option = array();
  if (!empty($args['type'])) {
    foreach ($args['type'] as $k => $v) {
      $type_option[$v['ID']] = t($v['name']);
    }
  }

  //list of location options
  $location_option = array();
  if (!empty($args['location'])) {
    foreach ($args['location'] as $k => $v) {
      $location_option[$v['ID']] = t($v['name']);
    }
  }


  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Meeting Name'),
    '#title_display' => 'hidden',
    '#size' => 255,
    '#maxlength' => 255,
    //'#default_value' => $default[ 'company_name' ],
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Meeting Name'
    ),
  );
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Meeting Type'),
    '#title_display' => 'hidden',
    '#options' => $type_option,
    //'#default_value' => $default[ 'company_name' ],
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
    ),

  );
  $form['location'] = array(
    '#type' => 'select',
    '#title' => t('Company Name'),
    '#title_display' => 'hidden',
    '#options' => $location_option,
    //'#default_value' => $default['company_name'],
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
    ),

  );
  $form['date'] = array(
    '#type' => 'textfield',
    '#title' => t('Meeting Date'),
    '#title_display' => 'hidden',
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Meeting Date'
    ),

  );

  /*$form['supplier'][0]['id[]'] = array(
    '#type' => 'checkbox',
    '#attributes' => array(
      'class' => array('form-control'),
    ),
    '#field_prefix' => '<div class="div-cell" style="width: 3%; text-align: center">',
    '#field_suffix' => '</div>',
  );
  $form['supplier'][0]['company_name[]'] = array(
    '#type' => 'textfield',
    '#title' => t('Company_name'),
    '#title_display' => 'hidden',
    '#size' => 255,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => t('Company_name'),
    ),
    '#field_prefix' => '<div class="div-cell">',
    '#field_suffix' => '</div>',
  );
  $form['supplier'][0]['firstname[]'] = array(
    '#type' => 'textfield',
    '#title' => t('First Name'),
    '#title_display' => 'hidden',
    '#size' => 255,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'First Name',
    ),
    '#field_prefix' => '<div class="div-cell">',
    '#field_suffix' => '</div>',
  );
  $form['supplier'][0]['lastname[]'] = array(
    '#type' => 'textfield',
    '#title' => t('Last Name'),
    '#title_display' => 'hidden',
    '#size' => 255,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Last Name',
    ),
    '#field_prefix' => '<div class="div-cell">',
    '#field_suffix' => '</div>',
  );
  $form['supplier'][0]['address[]'] = array(
    '#type' => 'textfield',
    '#title' => t('Address'),
    '#title_display' => 'hidden',
    '#size' => 255,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Address',
    ),
    '#field_prefix' => '<div class="div-cell">',
    '#field_suffix' => '</div>',
  );
  $form['supplier'][0]['country[]'] = array(
    '#type' => 'textfield',
    '#title' => t('Country'),
    '#title_display' => 'hidden',
    '#size' => 255,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Country',
    ),
    '#field_prefix' => '<div class="div-cell">',
    '#field_suffix' => '</div>',
  );
  $form['supplier'][0]['city[]'] = array(
    '#type' => 'textfield',
    '#title' => t('Meeting Name'),
    '#title_display' => 'hidden',
    '#size' => 255,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'City',
    ),
    '#field_prefix' => '<div class="div-cell">',
    '#field_suffix' => '</div>',
  );
  $form['supplier'][0]['email[]'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#title_display' => 'hidden',
    '#size' => 255,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Email',
    ),
    '#field_prefix' => '<div class="div-cell">',
    '#field_suffix' => '</div>',
  );
  $form['supplier'][0]['web_address[]'] = array(
    '#type' => 'textfield',
    '#title' => t('Website'),
    '#title_display' => 'hidden',
    '#size' => 255,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Website',
    ),
    '#field_prefix' => '<div class="div-cell">',
    '#field_suffix' => '</div>',
  );
  $form['supplier'][0]['twitter[]'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter'),
    '#title_display' => 'hidden',
    '#size' => 255,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Twitter',
    ),
    '#field_prefix' => '<div class="div-cell">',
    '#field_suffix' => '</div>',
  );
  $form['supplier'][0]['phone[]'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone'),
    '#title_display' => 'hidden',
    '#size' => 255,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Phone',
    ),
    '#field_prefix' => '<div class="div-cell">',
    '#field_suffix' => '</div>',
  );*/

  $i = 1;
  foreach ($args['supplier'] as $k => $v) {
    $form['supplier'][$i]['check[]'] = array(
      '#type' => 'checkbox',
      '#attributes' => array(
        'class' => array('form-control'),
      ),
      '#field_prefix' => '<div class="div-cell" style="width: 3%; text-align: center">',
      '#field_suffix' => '</div>',
    );
    $form['supplier'][$i]['id[]'] = array(
      '#type' => 'hidden',
      '#default_value' => $v['ID'],
      '#attributes' => array(
        'class' => array('form-control'),
      ),
      '#field_prefix' => '<div class="div-cell" style="width: 3%; text-align: center">',
      '#field_suffix' => '</div>',
    );
    $form['supplier'][$i]['company_name[]'] = array(
      '#type' => 'textfield',
      '#title' => t('Company_name'),
      '#title_display' => 'hidden',
      '#size' => 255,
      '#maxlength' => 255,
      '#default_value' => $v['company_name'],
      '#required' => TRUE,
      '#attributes' => array(
        'class' => array('form-control'),
        'placeholder' => t('Company_name'),
        'readonly' => TRUE,
      ),
      '#field_prefix' => '<div class="div-cell">',
      '#field_suffix' => '</div>',
    );
    $form['supplier'][$i]['firstname[]'] = array(
      '#type' => 'textfield',
      '#title' => t('First Name'),
      '#title_display' => 'hidden',
      '#size' => 255,
      '#maxlength' => 255,
      '#default_value' => $v['first_name'],
      '#required' => TRUE,
      '#attributes' => array(
        'class' => array('form-control'),
        'placeholder' => 'First Name',
        'readonly' => TRUE,
      ),
      '#field_prefix' => '<div class="div-cell">',
      '#field_suffix' => '</div>',
    );
    $form['supplier'][$i]['lastname[]'] = array(
      '#type' => 'textfield',
      '#title' => t('Last Name'),
      '#title_display' => 'hidden',
      '#size' => 255,
      '#maxlength' => 255,
      '#default_value' => $v['last_name'],
      '#required' => TRUE,
      '#attributes' => array(
        'class' => array('form-control'),
        'placeholder' => 'Last Name',
        'readonly' => TRUE,
      ),
      '#field_prefix' => '<div class="div-cell">',
      '#field_suffix' => '</div>',
    );
    $form['supplier'][$i]['address[]'] = array(
      '#type' => 'textfield',
      '#title' => t('Address'),
      '#title_display' => 'hidden',
      '#size' => 255,
      '#maxlength' => 255,
      '#default_value' => $v['address'],
      '#required' => TRUE,
      '#attributes' => array(
        'class' => array('form-control'),
        'placeholder' => 'Address',
        'readonly' => TRUE,
      ),
      '#field_prefix' => '<div class="div-cell">',
      '#field_suffix' => '</div>',
    );
    $form['supplier'][$i]['country[]'] = array(
      '#type' => 'textfield',
      '#title' => t('Country'),
      '#title_display' => 'hidden',
      '#size' => 255,
      '#maxlength' => 255,
      '#default_value' => $v['country'],
      '#required' => TRUE,
      '#attributes' => array(
        'class' => array('form-control'),
        'placeholder' => 'Country',
        'readonly' => TRUE,
      ),
      '#field_prefix' => '<div class="div-cell">',
      '#field_suffix' => '</div>',
    );
    $form['supplier'][$i]['city[]'] = array(
      '#type' => 'textfield',
      '#title' => t('Meeting Name'),
      '#title_display' => 'hidden',
      '#size' => 255,
      '#maxlength' => 255,
      '#default_value' => $v['city'],
      '#required' => TRUE,
      '#attributes' => array(
        'class' => array('form-control'),
        'placeholder' => 'City',
        'readonly' => TRUE,
      ),
      '#field_prefix' => '<div class="div-cell">',
      '#field_suffix' => '</div>',
    );
    $form['supplier'][$i]['email[]'] = array(
      '#type' => 'textfield',
      '#title' => t('Email'),
      '#title_display' => 'hidden',
      '#size' => 255,
      '#maxlength' => 255,
      '#default_value' => $v['email'],
      '#required' => TRUE,
      '#attributes' => array(
        'class' => array('form-control'),
        'placeholder' => 'Email',
        'readonly' => TRUE,
      ),
      '#field_prefix' => '<div class="div-cell">',
      '#field_suffix' => '</div>',
    );
    $form['supplier'][$i]['web_address[]'] = array(
      '#type' => 'textfield',
      '#title' => t('Website'),
      '#title_display' => 'hidden',
      '#size' => 255,
      '#maxlength' => 255,
      '#default_value' => $v['web_address'],
      '#required' => TRUE,
      '#attributes' => array(
        'class' => array('form-control'),
        'placeholder' => 'Website',
        'readonly' => TRUE,
      ),
      '#field_prefix' => '<div class="div-cell">',
      '#field_suffix' => '</div>',
    );
    $form['supplier'][$i]['twitter[]'] = array(
      '#type' => 'textfield',
      '#title' => t('Twitter'),
      '#title_display' => 'hidden',
      '#size' => 255,
      '#maxlength' => 255,
      '#default_value' => $v['twitter'],
      '#required' => TRUE,
      '#attributes' => array(
        'class' => array('form-control'),
        'placeholder' => 'Twitter',
        'readonly' => TRUE,
      ),
      '#field_prefix' => '<div class="div-cell">',
      '#field_suffix' => '</div>',
    );
    $form['supplier'][$i]['phone[]'] = array(
      '#type' => 'textfield',
      '#title' => t('Phone'),
      '#title_display' => 'hidden',
      '#size' => 255,
      '#maxlength' => 255,
      '#default_value' => $v['phone'],
      '#required' => TRUE,
      '#attributes' => array(
        'class' => array('form-control'),
        'placeholder' => 'Phone',
        'readonly' => TRUE,
      ),
      '#field_prefix' => '<div class="div-cell">',
      '#field_suffix' => '</div>',
    );
    $i++;
  }


  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#attributes' => array('class' => array('btn', 'btn-xs', 'btn-success')),
    '#prefix' => '<div class="col-lg-12 text-right" >',
    '#suffix' => '</div>',
  );

  $form['#validate'][] = 'form_meeting_create_validate';

  return $form;
}


/**
 * @param $form
 * @param $form_state
 */
function form_meeting_create_validate($form, &$form_state) {
  //including validation class
  include_once drupal_get_path('module',
      'petty_cash') . '/classes/GUMP/gump.class.php';
  $meeting = $supplier = new GUMP();
  $meeting->validation_rules(array(
    'name' => 'required|alpha_numeric|max_len,50|min_len,3',
    'type' => 'required|numeric',
    'location' => 'required|numeric',
    'date' => 'required|date'
  ));
  $validate = $meeting->run($form_state['input']);

  /*$supplier->validation_rules(array(
    'company_name' => 'required|alpha_numeric|max_len,50|min_len,3',
    'first_name' => 'required|alpha|max_len,50|min_len,3',
    'last_name' => 'required|alpha|max_len,50|min_len,3',
    'address' => 'required|street_address|max_len,255|min_len,3',
    'country' => 'required|alpha|max_len,50|min_len,3',
    'city' => 'required|alpha|max_len,50|min_len,3',
    'email' => 'required|valid_email',
    'phone' => 'required|regex,/[\d]/|max_len,50|min_len,3',
    'web_address' => 'required|max_len,255|min_len,5',
    'twitter' => 'required|valid_url',
  ));

  $validate = $supplier->run($form_state['input']);*/

  if ($validate === FALSE) {
    foreach ($meeting->get_errors_array() as $field => $err_msg) {
      form_set_error($field, t($err_msg));
    }
  }
}

/**
 * @param $form
 * @param $form_state
 */
function form_meeting_create_submit($form, &$form_state) {

  global $user;
  db_insert('meeting')
    ->fields(array(
      'meeting_name' => $form_state['input']['name'],
      'date' => $form_state['input']['date'],
      'UID_fk' => $user->uid,
      'MEETING_TYPE_fk' => $form_state['input']['type'],
      'LOCATION_fk' => $form_state['input']['location'],
    ))
    ->execute();
  $lastId = Database::getConnection()->lastInsertId();
  if (isset($form_state['input']['id']) && !empty($form_state['input']['id'])) {

    $count = count($form_state['input']['id']);
    for ($i = 0; $i < $count; $i++) {
      if ($form_state['input']['id'][$i] != '') {
        db_insert('supplier_meeting')
          ->fields(array(
            'UID_fk' => $user->uid,
            'MEETING_fk' => $lastId,
            'SUPPLIER_fk' => $form_state['input']['id'][$i],
          ))
          ->execute();
      }
    }
  }


  drupal_set_message('Supplier Added');
}

/*********************/

function create_meeting_type() {
  var_dump(__FUNCTION__);
}

function update_meeting() {
  var_dump(__FUNCTION__);
}