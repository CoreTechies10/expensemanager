<?php

function add_edit() {
  global $user;
  $fields = array(
    'meeting_type' => array(),
    'location' => array(),
    'category' => array(),
    'payment' => array(),
  );

  $fields['meeting_type'] = drupal_get_form('form_meeting_type');
  $fields['location'] = drupal_get_form('form_location');
  $fields['category'] = drupal_get_form('form_category');
  $fields['payment'] = drupal_get_form('form_payment');

  $fields['meeting_type_edit'] = drupal_get_form('form_meeting_type_edit');
  $fields['location_edit'] = drupal_get_form('form_location_edit');
  $fields['category_edit'] = drupal_get_form('form_category_edit');
  $fields['payment_edit'] = drupal_get_form('form_payment_edit');


  $fields['meeting_type_list'] = $fields['profile'] = db_select('meeting_type', 'm')
    ->fields('m')
    ->where('UID_fk = :uid', array(':uid' => $user->uid))
    ->execute()
    ->fetchAllAssoc('ID', PDO::FETCH_ASSOC);

  $fields['location_list'] = $fields['profile'] = db_select('location', 'm')
    ->fields('m')
    ->where('UID_fk = :uid', array(':uid' => $user->uid))
    ->execute()
    ->fetchAllAssoc('ID', PDO::FETCH_ASSOC);

  $fields['category_list'] = $fields['profile'] = db_select('category', 'm')
    ->fields('m')
    ->where('UID_fk = :uid', array(':uid' => $user->uid))
    ->execute()
    ->fetchAllAssoc('ID', PDO::FETCH_ASSOC);

  $fields['payment_list'] = $fields['profile'] = db_select('payments_type', 'm')
    ->fields('m')
    ->where('UID_fk = :uid', array(':uid' => $user->uid))
    ->execute()
    ->fetchAllAssoc('ID', PDO::FETCH_ASSOC);

  return theme('add_edit', $fields);
}

/************/
function form_meeting_type($form, &$form_state) {
  $form['meeting_name'] = array(
    '#type' => 'textfield',
    '#title' => t(''),
    '#title_display' => 'hidden',
    '#size' => 20,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#attributes' => array(
      'placeholder' => 'Meeting Type'
    ),

  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#attributes' => array('class' => array('btn', 'btn-xs', 'btn-success')),
  );
  return $form;
}

//Validating from
function form_meeting_type_validate($form, &$form_state) {

  //including validation class
  include_once drupal_get_path('module', 'petty_cash') . '/classes/GUMP/gump.class.php';
  $gump = new GUMP();
  $gump->validation_rules(array(
    'meeting_name' => 'required|alpha_numeric|max_len,50|min_len,3',
  ));

  $validate = $gump->run($form_state['values']);

  if ($validate === FALSE) {
    foreach ($gump->get_errors_array() as $field => $err_msg) {
      form_set_error($field, t($err_msg));
    }
  }

}

//Submitting Form
function form_meeting_type_submit($form, &$form_state) {
  global $user;
  try {
    db_insert('meeting_type')
      ->fields(array(
        'name' => $form_state['values']['meeting_name'],
        'UID_fk' => $user->uid,
      ))
      ->execute();
  } catch (Exception $e) {
    $error = $e->getMessage();
    watchdog_exception('petty_cash', $e, t('Caught an error: ' . $error));
    firep($e);
  }
  drupal_set_message('Supplier Added');
}


/*************/
function form_location($form, &$form_state) {
  $form['location_name'] = array(
    '#type' => 'textfield',
    '#title' => t(''),
    '#title_display' => 'hidden',
    '#size' => 20,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#attributes' => array(
      'placeholder' => 'Location'
    ),

  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#attributes' => array('class' => array('btn', 'btn-xs', 'btn-success')),
  );
  return $form;
}

//Validating from
function form_location_validate($form, &$form_state) {

  //including validation class
  include_once drupal_get_path('module', 'petty_cash') . '/classes/GUMP/gump.class.php';
  $gump = new GUMP();
  $gump->validation_rules(array(
    'location_name' => 'required|alpha_numeric|max_len,50|min_len,3',
  ));

  $validate = $gump->run($form_state['values']);

  if ($validate === FALSE) {
    foreach ($gump->get_errors_array() as $field => $err_msg) {
      form_set_error($field, t($err_msg));
    }
  }

}

//Submitting Form
function form_location_submit($form, &$form_state) {
  global $user;
  try {
    db_insert('location')
      ->fields(array(
        'name' => $form_state['values']['location_name'],
        'UID_fk' => $user->uid,
      ))
      ->execute();
  } catch (Exception $e) {
    $error = $e->getMessage();
    watchdog_exception('petty_cash', $e, t('Caught an error: ' . $error));
  }
  drupal_set_message('Supplier Added');
}


/***************/
function form_category($form, &$form_state) {
  $form['category_name'] = array(
    '#type' => 'textfield',
    '#title' => t(''),
    '#title_display' => 'hidden',
    '#size' => 20,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#attributes' => array(
      'placeholder' => 'Category'
    ),

  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#attributes' => array('class' => array('btn', 'btn-xs', 'btn-success')),
  );
  return $form;
}

//Validating from
function form_category_validate($form, &$form_state) {

  //including validation class
  include_once drupal_get_path('module', 'petty_cash') . '/classes/GUMP/gump.class.php';
  $gump = new GUMP();
  $gump->validation_rules(array(
    'category_name' => 'required|alpha_numeric|max_len,50|min_len,3',
  ));

  $validate = $gump->run($form_state['values']);

  if ($validate === FALSE) {
    foreach ($gump->get_errors_array() as $field => $err_msg) {
      form_set_error($field, t($err_msg));
    }
  }

}

//Submitting Form
function form_category_submit($form, &$form_state) {
  global $user;
  try {
    db_insert('category')
      ->fields(array(
        'name' => $form_state['values']['category_name'],
        'UID_fk' => $user->uid,
      ))
      ->execute();
  } catch (Exception $e) {
    $error = $e->getMessage();
    watchdog_exception('petty_cash', $e, t('Caught an error: ' . $error));
  }
  drupal_set_message('Supplier Added');
}


/****************/
function form_payment($form, &$form_state) {
  $form['payment_name'] = array(
    '#type' => 'textfield',
    '#title' => t(''),
    '#title_display' => 'hidden',
    '#size' => 20,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#attributes' => array(
      'placeholder' => 'Payment'
    ),

  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#attributes' => array('class' => array('btn', 'btn-xs', 'btn-success')),
  );
  return $form;
}

//Validating from
function form_payment_validate($form, &$form_state) {

  //including validation class
  include_once drupal_get_path('module', 'petty_cash') . '/classes/GUMP/gump.class.php';
  $gump = new GUMP();
  $gump->validation_rules(array(
    'payment_name' => 'required|alpha_numeric|max_len,50|min_len,3',
  ));

  $validate = $gump->run($form_state['values']);

  if ($validate === FALSE) {
    foreach ($gump->get_errors_array() as $field => $err_msg) {
      form_set_error($field, t($err_msg));
    }
  }

}

//Submitting Form
function form_payment_submit($form, &$form_state) {
  global $user;
  try {
    db_insert('payments_type')
      ->fields(array(
        'payments_name' => $form_state['values']['payment_name'],
        'UID_fk' => $user->uid,
      ))
      ->execute();
  } catch (Exception $e) {
    $error = $e->getMessage();
    watchdog_exception('petty_cash', $e, t('Caught an error: ' . $error));
  }
  drupal_set_message('Supplier Added');
}


/********************/
function form_meeting_type_edit($form, &$form_state) {
  $form['meeting_name_edit'] = array(
    '#type' => 'hidden',
  );
  $form['id'] = array(
    '#type' => 'hidden',
  );
  $form['type'] = array(
    '#type' => 'hidden',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#attributes' => array('class' => array('hidden',)),
  );
  return $form;
}

function form_meeting_type_edit_submit($form, &$form_state) {
  global $user;
  if ($form_state['values']['type'] == 'update') {
    db_update('meeting_type')
      ->fields(array('name' => $form_state['values']['meeting_name_edit']))
      ->condition('UID_fk', $user->uid, '=')
      ->condition('id', $form_state['values']['id'], '=')
      ->execute();
    drupal_set_message('Meeting Type Updated');
  }
  elseif ($form_state['values']['type'] == 'delete') {
    db_delete('meeting_type')
      ->condition('UID_fk', $user->uid, '=')
      ->condition('id', $form_state['values']['id'], '=')
      ->execute();
    drupal_set_message('Meeting Deleted');
  }
}

/********************/
function form_location_edit($form, &$form_state) {
  $form['location_name_edit'] = array(
    '#type' => 'hidden',
  );
  $form['id'] = array(
    '#type' => 'hidden',
  );
  $form['type'] = array(
    '#type' => 'hidden',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#attributes' => array('class' => array('hidden',)),
  );
  return $form;
}

function form_location_edit_submit($form, &$form_state) {
  global $user;
  if ($form_state['values']['type'] == 'update') {
    db_update('location')
      ->fields(array('name' => $form_state['values']['location_name_edit']))
      ->condition('UID_fk', $user->uid, '=')
      ->condition('id', $form_state['values']['id'], '=')
      ->execute();
    drupal_set_message('Location Updated');
  }
  elseif ($form_state['values']['type'] == 'delete') {
    db_delete('location')
      ->condition('UID_fk', $user->uid, '=')
      ->condition('id', $form_state['values']['id'], '=')
      ->execute();
    drupal_set_message('Location Updated');
  }
}

/********************/
function form_category_edit($form, &$form_state) {
  $form['category_name_edit'] = array(
    '#type' => 'hidden',
  );
  $form['id'] = array(
    '#type' => 'hidden',
  );
  $form['type'] = array(
    '#type' => 'hidden',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#attributes' => array('class' => array('hidden',)),
  );
  return $form;
}

function form_category_edit_submit($form, &$form_state) {
  global $user;

  db_update('category')
    ->fields(array('name' => $form_state['values']['category_name_edit']))
    ->condition('UID_fk', $user->uid, '=')
    ->condition('id', $form_state['values']['id'], '=')
    ->execute();
  drupal_set_message('Category Updated');
}

/********************/
function form_payment_edit($form, &$form_state) {
  $form['payment_name_edit'] = array(
    '#type' => 'hidden',
  );
  $form['id'] = array(
    '#type' => 'hidden',
  );
  $form['type'] = array(
    '#type' => 'hidden',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#attributes' => array('class' => array('hidden',)),
  );
  return $form;
}

function form_payment_edit_submit($form, &$form_state) {
  global $user;

  db_update('payments_type')
    ->fields(array('name' => $form_state['values']['payment_name_edit']))
    ->condition('UID_fk', $user->uid, '=')
    ->condition('id', $form_state['values']['id'], '=')
    ->execute();
  drupal_set_message('Payments Type Updated');
}

