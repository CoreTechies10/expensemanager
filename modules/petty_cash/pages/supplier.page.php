<?php
/**
 * Created by PhpStorm.
 * User: hitesh
 * Date: 23-9-15
 * Time: 12:31
 */

//Handing Request
/**
 * @return array|string
 * @throws \Exception
 * @call form_create_supplier
 */
function supplier_list() {
  $args = func_get_args();
  $form = array();
  $fields = array(
    'profile' => '',
    'form' => '',
    'edit_form' => '',
    'update_form' => '',
    'balance' => array(),
    'meetings' => array(),
    'categories' => array(),
    'payment_types' => array(),
    'delivery' => '',
    'paid_amount' => '',
    'upcoming_meeting' => '',
    'events' => '',
  );
  global $user;

  if (!empty($args) && is_numeric($args[0])) {
    #taking list of supplier
    $fields['profile'] = db_select('supplier', 's')
      ->fields('s')
      ->where('UID_fk = :uid AND ID = :id',
        array(':uid' => $user->uid, ':id' => $args[0]))
      ->execute()
      ->fetchAssoc();

    # Balance list
    $count = db_query('SELECT COUNT(*) FROM {supplier_payments} WHERE SUPPLIER_fk = :supplier And UID_fk = :uid ', array(
      ':supplier' => $args[0],
      ':uid' => $user->uid
    ))->fetchAssoc();
    if ($count['COUNT(*)'] > 0) {
      $balance = db_select('supplier_payments', 'p');
      $balance->join('category', 'c', 'c.ID = p.CATEGORY_fk');
      $balance->join('supplier', 's', 's.ID = p.SUPPLIER_fk');
      $balance->join('meeting', 'm', 'm.ID = p.MEETING_fk');
      $balance->join('payments_type', 'pt', 'pt.ID = p.PAYMENT_TYPE_fk');
      $balance->join('category', 'ct', 'ct.ID = p.CATEGORY_fk');
      $fields['balance'] = $balance->fields('p')
        ->fields('c', array('name'))
        ->fields('s', array('company_name', 'first_name', 'last_name'))
        ->fields('m', array('meeting_name', 'date'))
        ->fields('pt', array('payments_name',))
        ->where('p.SUPPLIER_fk = :supplier And p.UID_fk = :uid', array(':supplier' => $args[0], ':uid' => $user->uid))
        ->execute()
        ->fetchAllAssoc('ID', PDO::FETCH_ASSOC);
    }

    $fields['meetings'] = db_select('meeting', 'm');
    $fields['meetings'] = $fields['meetings']->fields('m')
      ->condition('UID_fk', $user->uid)
      ->execute()
      ->fetchAllAssoc('ID', PDO::FETCH_ASSOC);

    $fields['categories'] = db_select('category', 'c');
    $fields['categories'] = $fields['categories']->fields('c')
      ->condition('UID_fk', $user->uid)
      ->execute()
      ->fetchAllAssoc('ID', PDO::FETCH_ASSOC);
    $fields['payment_types'] = db_select('payments_type', 'p');
    $fields['payment_types'] = $fields['payment_types']->fields('p')
      ->condition('UID_fk', $user->uid)
      ->execute()
      ->fetchAllAssoc('ID', PDO::FETCH_ASSOC);

    $theme = 'supplier_profile';
    $fields['form'] = drupal_get_form('form_create_supplier', $fields['form']);
    $fields['edit_form'] = drupal_get_form('form_supplier_edit');
    $fields['update_form'] = drupal_get_form('form_update_supplier', $args[0]);

  }
  else {
    $fields['profile'] = db_select('supplier', 's')
      ->fields('s')
      ->where('UID_fk = :uid', array(':uid' => $user->uid))
      ->execute()
      ->fetchAllAssoc('ID', PDO::FETCH_ASSOC);

    if (empty($fields['profile'])) {
      $form['empty'] = array(
        '#markup' => 'Sorry there is no suppler. Please go to create for create supplier',
      );
      return $form;
    }

    $theme = 'supplier_list';
    $fields['form'] = drupal_get_form('form_create_supplier');
  }

  return theme($theme, $fields);
}

/************************************/

//Handling Request
function create_supplier() {
  $form = drupal_get_form('form_create_supplier');
  return theme('supplier_create', array(
    'form' => $form,
  ));
}

/**
 * @param $form
 * @param $form_state
 * @return mixed
 * @see create_supplier
 * @see supplier_list
 */
//Creating form
function form_create_supplier($form, &$form_state) {

  $default = array(
    'ID' => '',
    'company_name' => '',
    'first_name' => '',
    'last_name' => '',
    'address' => '',
    'country' => '',
    'city' => '',
    'email' => '',
    'phone' => '',
    'web_address' => '',
    'twitter' => '',
  );

  if (!empty($form_state['build_info']['args'][0])) {
    $value = $form_state['build_info']['args'][0];
    foreach ($default as $k => $v) {
      if (isset($value[$k])) {
        $default[$k] = $value[$k];
      }
    }
  }
  if (!empty ($default['ID'])) {
    $form['company_name'] = array(
      '#type' => 'hidden',
      '#value' => $default['ID'],
    );
  }

  $form['company_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Company Name'),
    '#size' => 60,
    '#maxlength' => 255,
    '#default_value' => $default['company_name'],
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Company Name'
    ),
    '#field_prefix' => '<div class="col-lg-8">',
    '#field_suffix' => '</div><div class="clearfix"></div>',
    '#title_prefix_suffix' => TRUE, //apply label class
    '#title_prefix' => '<div class="col-lg-4">',
    '#title_suffix' => '</div>',
    '#title_attributes' => 'control-label' //now only class
  );
  $form['first_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Contact F-Name'),
    '#size' => 60,
    '#maxlength' => 128,
    '#default_value' => $default['first_name'],
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Contact First Name'
    ),
    '#field_prefix' => '<div class="col-lg-8">',
    '#field_suffix' => '</div><div class="clearfix"></div>',
    '#title_prefix_suffix' => TRUE, //apply label class
    '#title_prefix' => '<div class="col-lg-4">',
    '#title_suffix' => '</div>',
    '#title_attributes' => 'control-label' //now only class
  );
  $form['last_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Contact L-Name'),
    '#size' => 60,
    '#maxlength' => 128,
    '#default_value' => $default['last_name'],
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Contact Last Name'
    ),
    '#field_prefix' => '<div class="col-lg-8">',
    '#field_suffix' => '</div><div class="clearfix"></div>',
    '#title_prefix_suffix' => TRUE, //apply label class
    '#title_prefix' => '<div class="col-lg-4">',
    '#title_suffix' => '</div>',
    '#title_attributes' => 'control-label' //now only class
  );
  $form['address'] = array(
    '#type' => 'textfield',
    '#title' => t('Address'),
    '#size' => 60,
    '#maxlength' => 128,
    '#default_value' => $default['address'],
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Address'
    ),
    '#field_prefix' => '<div class="col-lg-8">',
    '#field_suffix' => '</div><div class="clearfix"></div>',
    '#title_prefix_suffix' => TRUE, //apply label class
    '#title_prefix' => '<div class="col-lg-4">',
    '#title_suffix' => '</div>',
    '#title_attributes' => 'control-label' //now only class
  );
  $form['country'] = array(
    '#type' => 'textfield',
    '#title' => t('Country'),
    '#size' => 60,
    '#maxlength' => 128,
    '#default_value' => $default['country'],
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Country'
    ),
    '#field_prefix' => '<div class="col-lg-8">',
    '#field_suffix' => '</div><div class="clearfix"></div>',
    '#title_prefix_suffix' => TRUE, //apply label class
    '#title_prefix' => '<div class="col-lg-4">',
    '#title_suffix' => '</div>',
    '#title_attributes' => 'control-label' //now only class
  );
  $form['city'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#size' => 60,
    '#maxlength' => 128,
    '#default_value' => $default['city'],
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'city'
    ),
    '#field_prefix' => '<div class="col-lg-8">',
    '#field_suffix' => '</div><div class="clearfix"></div>',
    '#title_prefix_suffix' => TRUE, //apply label class
    '#title_prefix' => '<div class="col-lg-4">',
    '#title_suffix' => '</div>',
    '#title_attributes' => 'control-label' //now only class
  );
  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#size' => 60,
    '#maxlength' => 128,
    '#default_value' => $default['email'],
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Email'
    ),
    '#field_prefix' => '<div class="col-lg-8">',
    '#field_suffix' => '</div><div class="clearfix"></div>',
    '#title_prefix_suffix' => TRUE, //apply label class
    '#title_prefix' => '<div class="col-lg-4">',
    '#title_suffix' => '</div>',
    '#title_attributes' => 'control-label' //now only class
  );
  $form['phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Tel No.'),
    '#size' => 60,
    '#maxlength' => 128,
    '#default_value' => $default['phone'],
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Telephone Number'
    ),
    '#field_prefix' => '<div class="col-lg-8">',
    '#field_suffix' => '</div><div class="clearfix"></div>',
    '#title_prefix_suffix' => TRUE, //apply label class
    '#title_prefix' => '<div class="col-lg-4">',
    '#title_suffix' => '</div>',
    '#title_attributes' => 'control-label' //now only class
  );
  $form['web_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Website Address'),
    '#size' => 60,
    '#maxlength' => 128,
    '#default_value' => $default['web_address'],
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Website Address'
    ),
    '#field_prefix' => '<div class="col-lg-8">',
    '#field_suffix' => '</div><div class="clearfix"></div>',
    '#title_prefix_suffix' => TRUE, //apply label class
    '#title_prefix' => '<div class="col-lg-4">',
    '#title_suffix' => '</div>',
    '#title_attributes' => 'control-label' //now only class
  );
  $form['twitter'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter ID'),
    '#size' => 60,
    '#maxlength' => 128,
    '#default_value' => $default['twitter'],
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Twitter ID'
    ),
    '#field_prefix' => '<div class="col-lg-8">',
    '#field_suffix' => '</div><div class="clearfix"></div>',
    '#title_prefix_suffix' => TRUE, //apply label class
    '#title_prefix' => '<div class="col-lg-4">',
    '#title_suffix' => '</div>',
    '#title_attributes' => 'control-label' //now only class
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('+ Add'),
    '#attributes' => array('class' => array('btn', 'btn-xs', 'btn-success')),
    '#prefix' => '<div class="col-lg-12 text-right" >',
    '#suffix' => '</div><div class="clearfix"></div>',
  );

  $form['#validate'][] = 'form_create_supplier_validate';
  return $form;
}

//Validating from
function form_create_supplier_validate($form, &$form_state) {

  //including validation class
  include_once drupal_get_path('module',
      'petty_cash') . '/classes/GUMP/gump.class.php';
  $gump = new GUMP();
  $gump->validation_rules(array(
    'company_name' => 'required|alpha_numeric|max_len,50|min_len,3',
    'first_name' => 'required|alpha|max_len,50|min_len,3',
    'last_name' => 'required|alpha|max_len,50|min_len,3',
    'address' => 'required|street_address|max_len,255|min_len,3',
    'country' => 'required|alpha|max_len,50|min_len,3',
    'city' => 'required|alpha|max_len,50|min_len,3',
    'email' => 'required|valid_email',
    'phone' => 'required|regex,/[\d]/|max_len,50|min_len,3',
    'web_address' => 'required|max_len,255|min_len,5',
    'twitter' => 'required|valid_url',
  ));

  $validate = $gump->run($form_state['values']);

  if ($validate === FALSE) {
    foreach ($gump->get_errors_array() as $field => $err_msg) {
      form_set_error($field, t($err_msg));
    }
  }

}

//Submitting Form
function form_create_supplier_submit($form, &$form_state) {
  global $user;
  try {
    db_insert('supplier')
      ->fields(array(
        'company_name' => $form_state['values']['company_name'],
        'first_name' => $form_state['values']['first_name'],
        'last_name' => $form_state['values']['last_name'],
        'address' => $form_state['values']['address'],
        'country' => $form_state['values']['country'],
        'city' => $form_state['values']['city'],
        'email' => $form_state['values']['email'],
        'phone' => $form_state['values']['phone'],
        'web_address' => $form_state['values']['web_address'],
        'twitter' => $form_state['values']['twitter'],
        'UID_fk' => $user->uid,
      ))
      ->execute();
  } catch (Exception $e) {
    $error = $e->getMessage();
    watchdog_exception('petty_cash', $e, t('Caught an error: ' . $error));
    firep($e);
  }
  drupal_set_message('Supplier Added');
}


/******************************/

function form_update_supplier($form, &$form_state, $id=null) {

  $form['id'] = array(
    '#type' => 'hidden',
    '#value' => $id
  );
  $form['last_name'] = array(
    '#type' => 'hidden',
  );
  $form['first_name'] = array(
    '#type' => 'hidden',
  );
  $form['address'] = array(
    '#type' => 'hidden',
  );
  $form['country'] = array(
    '#type' => 'hidden',
  );
  $form['city'] = array(
    '#type' => 'hidden',
  );
  $form['email'] = array(
    '#type' => 'hidden',
  );
  $form['phone'] = array(
    '#type' => 'hidden',
  );
  $form['web_address'] = array(
    '#type' => 'hidden',
  );
  $form['twitter'] = array(
    '#type' => 'hidden',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#attributes' => array('class' => array('hidden',)),
  );
  return $form;
}

function form_update_supplier_submit($form, &$form_state) {
  global $user;
  $default = array(
    'company_name' => '',
    'first_name' => '',
    'last_name' => '',
    'address' => '',
    'country' => '',
    'city' => '',
    'email' => '',
    'phone' => '',
    'web_address' => '',
    'twitter' => '',
  );
  $fields = array();
  foreach ($form_state['values'] as $key => $val) {
    if (isset($default[$key]) && $val != '') {
      $fields[$key] = $val;
    }
  }

  if (!empty($fields)) {
    db_update('supplier')
      ->fields($fields)
      ->condition('ID', $form_state['values']['id'], '=')
      ->condition('UID_fk', $user->uid, '=')
      ->execute();
    drupal_set_message('Updated');
  }


}

/******************************/
function form_supplier_edit($form, &$form_state) {
  $form['id'] = array(
    '#type' => 'hidden',
  );
  $form['type'] = array(
    '#type' => 'hidden',
  );
  $form['value'] = array(
    '#type' => 'hidden',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#attributes' => array('class' => array('hidden',)),
  );
  return $form;
}

function form_supplier_edit_submit($form, &$form_state) {
  $types = explode('_', $form_state['values']['type']); // offset 0: field(delivery, amount, upcoming) offset 1: db table

  switch ($types[0]) {
    case 'delivery':
      switch ($types[1]) {
        case 'date':
          $time = strtotime($form_state['values']['value']);
          db_update('supplier_payments')
            ->fields(array('date' => date('Y-m-d H:i:s', $time)))
            ->condition('id', $form_state['values']['id'])
            ->execute();
          drupal_set_message('Date updated');

          break;
        case 'meeting':
          db_update('supplier_payments')
            ->fields(array('MEETING_fk' => $form_state['values']['value']))
            ->condition('id', $form_state['values']['id'])
            ->execute();
          drupal_set_message('Meeting updated');
          break;
        case 'category':
          db_update('supplier_payments')
            ->fields(array('CATEGORY_fk' => $form_state['values']['value']))
            ->condition('id', $form_state['values']['id'])
            ->execute();
          drupal_set_message('Category updated');
          break;
        case 'payment':
          db_update('supplier_payments')
            ->fields(array('PAYMENT_TYPE_fk' => $form_state['values']['value']))
            ->condition('id', $form_state['values']['id'])
            ->execute();
          drupal_set_message('Payment type updated');
          break;
        case 'amount':
          db_update('supplier_payments')
            ->fields(array('amount' => $form_state['values']['value']))
            ->condition('id', $form_state['values']['id'])
            ->execute();
          drupal_set_message('Amount updated');
          break;
      }
      break;
    case '':
      break;
  }
}

