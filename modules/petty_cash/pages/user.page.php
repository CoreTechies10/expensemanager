<?php
/**
 * Created by PhpStorm.
 * User: hitesh
 * Date: 2-10-15
 * Time: 14:48
 */

/**
 * Overridden the user login system
 */
function user_login_system() {

  global $user;
  if ($user->uid) {

    menu_set_active_item('user/' . $user->uid);
    return menu_execute_active_handler(NULL, FALSE);
  }
  else {
    $form = drupal_get_form('user_login');
    //user name field
    $form['name']['#attributes'] = array(
      'class' => array('form-control'),
      'placeholder' => 'Company Name',
      'required' => 'required',
    );
    $form['name']['#description'] = '';
    $form['name']['#field_prefix'] = '<div class="col-lg-8">';
    $form['name']['#field_suffix'] = '</div><div class="clearfix"></div>';
    $form['name']['#title_prefix_suffix'] = TRUE;
    $form['name']['#title_prefix'] = '<div class="col-lg-4">';
    $form['name']['#title_suffix'] = '</div>';
    $form['name']['#title_attributes'] = 'control-label';

    //user password field
    $form['pass']['#attributes'] = array(
      'class' => array('form-control'),
      'placeholder' => 'Password',
      'required' => 'required',
    );
    $form['pass']['#description'] = '';
    $form['pass']['#field_prefix'] = '<div class="col-lg-8">';
    $form['pass']['#field_suffix'] = '</div><div class="clearfix"></div>';
    $form['pass']['#title_prefix_suffix'] = TRUE;
    $form['pass']['#title_prefix'] = '<div class="col-lg-4">';
    $form['pass']['#title_suffix'] = '</div>';
    $form['pass']['#title_attributes'] = 'control-label';

    //user submit button
    $form['actions']['submit']['#attributes'] = array('class' => array('btn', 'btn-xs', 'btn-success'));
    $form['actions']['submit']['#prefix'] = '<div class="col-lg-12 text-right" > <span><a class="btn btn-xs btn-info" href="/user/register">Register</a></span>  ';
    $form['actions']['submit']['#suffix'] = '</div><div class="clearfix"></div>';

    return theme('user_login', array('form' => $form));
  }
}


/**
 * Overridden user register system
 */

function user_register_system() {
  $form = drupal_get_form('form_user_register_system');
  return $form;
}

function form_user_register_system($form, &$form_state) {
  $form = array();
  $form['username'] = array(
    '#type' => 'textfield',
    '#title' => t('User Name'),
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'User Name'
    ),
    '#field_prefix' => '<div class="col-lg-8">',
    '#field_suffix' => '</div><div class="clearfix"></div>',
    '#title_prefix_suffix' => TRUE, //apply label class
    '#title_prefix' => '<div class="col-lg-4">',
    '#title_suffix' => '</div>',
    '#title_attributes' => 'control-label' //now only class
  );
  $form['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Password'
    ),
    '#field_prefix' => '<div class="col-lg-8">',
    '#field_suffix' => '</div><div class="clearfix"></div>',
    '#title_prefix_suffix' => TRUE, //apply label class
    '#title_prefix' => '<div class="col-lg-4">',
    '#title_suffix' => '</div>',
    '#title_attributes' => 'control-label' //now only class
  );
  $form['company_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Company Name'),
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Company Name'
    ),
    '#field_prefix' => '<div class="col-lg-8">',
    '#field_suffix' => '</div><div class="clearfix"></div>',
    '#title_prefix_suffix' => TRUE, //apply label class
    '#title_prefix' => '<div class="col-lg-4">',
    '#title_suffix' => '</div>',
    '#title_attributes' => 'control-label' //now only class
  );
  $form['address'] = array(
    '#type' => 'textfield',
    '#title' => t('Address'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Address'
    ),
    '#field_prefix' => '<div class="col-lg-8">',
    '#field_suffix' => '</div><div class="clearfix"></div>',
    '#title_prefix_suffix' => TRUE, //apply label class
    '#title_prefix' => '<div class="col-lg-4">',
    '#title_suffix' => '</div>',
    '#title_attributes' => 'control-label' //now only class
  );
  $form['country'] = array(
    '#type' => 'textfield',
    '#title' => t('Country'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Country'
    ),
    '#field_prefix' => '<div class="col-lg-8">',
    '#field_suffix' => '</div><div class="clearfix"></div>',
    '#title_prefix_suffix' => TRUE, //apply label class
    '#title_prefix' => '<div class="col-lg-4">',
    '#title_suffix' => '</div>',
    '#title_attributes' => 'control-label' //now only class
  );
  $form['city'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'city'
    ),
    '#field_prefix' => '<div class="col-lg-8">',
    '#field_suffix' => '</div><div class="clearfix"></div>',
    '#title_prefix_suffix' => TRUE, //apply label class
    '#title_prefix' => '<div class="col-lg-4">',
    '#title_suffix' => '</div>',
    '#title_attributes' => 'control-label' //now only class
  );
  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Email'
    ),
    '#field_prefix' => '<div class="col-lg-8">',
    '#field_suffix' => '</div><div class="clearfix"></div>',
    '#title_prefix_suffix' => TRUE, //apply label class
    '#title_prefix' => '<div class="col-lg-4">',
    '#title_suffix' => '</div>',
    '#title_attributes' => 'control-label' //now only class
  );
  $form['phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Tel No.'),
    '#size' => 60,
    '#maxlength' => 128,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Telephone Number'
    ),
    '#field_prefix' => '<div class="col-lg-8">',
    '#field_suffix' => '</div><div class="clearfix"></div>',
    '#title_prefix_suffix' => TRUE, //apply label class
    '#title_prefix' => '<div class="col-lg-4">',
    '#title_suffix' => '</div>',
    '#title_attributes' => 'control-label' //now only class
  );
  $form['web_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Website Address'),
    '#size' => 60,
    '#maxlength' => 128,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Website Address'
    ),
    '#field_prefix' => '<div class="col-lg-8">',
    '#field_suffix' => '</div><div class="clearfix"></div>',
    '#title_prefix_suffix' => TRUE, //apply label class
    '#title_prefix' => '<div class="col-lg-4">',
    '#title_suffix' => '</div>',
    '#title_attributes' => 'control-label' //now only class
  );
  $form['twitter'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter ID'),
    '#size' => 60,
    '#maxlength' => 128,
    '#attributes' => array(
      'class' => array('form-control'),
      'placeholder' => 'Twitter ID'
    ),
    '#field_prefix' => '<div class="col-lg-8">',
    '#field_suffix' => '</div><div class="clearfix"></div>',
    '#title_prefix_suffix' => TRUE, //apply label class
    '#title_prefix' => '<div class="col-lg-4">',
    '#title_suffix' => '</div>',
    '#title_attributes' => 'control-label' //now only class
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#attributes' => array('class' => array('btn', 'btn-sm', 'btn-success')),
    '#prefix' => '<div class="col-lg-12 text-right" >',
    '#suffix' => '</div><div class="clearfix"></div>',
  );

  $form['#validate'][] = 'form_user_register_system_validate';
  return $form;
}

function form_user_register_system_validate($form, &$form_state) {
//including validation class
  include_once drupal_get_path('module', 'petty_cash') . '/classes/GUMP/gump.class.php';
  $gump = new GUMP();
  $gump->validation_rules(array(
    'username' => 'required|alpha|max_len,50|min_len,3',
    'password' => 'required|alpha_numeric|max_len,50|min_len,3',
    'company_name' => 'required|max_len,50|min_len,3',
    'address' => 'required|street_address|max_len,255|min_len,3',
    'country' => 'required|alpha|max_len,50|min_len,3',
    'city' => 'required|alpha|max_len,50|min_len,3',
    'email' => 'required|valid_email',
    'phone' => 'regex,/[\d]/|max_len,50',
    'web_address' => 'max_len,255',
    'twitter' => 'valid_url',
  ));

  $validate = $gump->run($form_state['values']);

  if ($validate === FALSE) {
    foreach ($gump->get_errors_array() as $field => $err_msg) {
      form_set_error($field, t($err_msg));
    }
  }
}

function form_user_register_system_submit($form, &$form_state) {

  $value = $form_state['values'];
  $user = user_save(NULL, array(
    'name' => $value['username'],
    'pass' => $value['password'],
    'mail' => $value['email'],
    'status' => FALSE,
    'data' => array(
      'company' => $value['company_name'],
      'address' => $value['address'],
      'country' => $value['country'],
      'city' => $value['city'],
      'phone' => $value['phone'],
      'web_address' => $value['web_address'],
      'twitter' => $value['twitter'],
    )
  ));

  drupal_set_message('Registration Done. Please check your email for farther process');
  $form_state['redirect'] = '';

  //drupal_mail('petty_cash', 'register_pending_email_auth', variable_get('site_mail', ini_get('sendmail_from')), language_default(), $user);
}


/**
 * Overridden user profile page
 */

function user_profile_page($account) {
  global $user;
  $form = drupal_get_form('user_profile_edit');
  $paypal = array();//$paypal = drupal_get_form('_user_payment');

  $balance = db_select('supplier_payments', 'p');
  $balance->join('category', 'c', 'c.ID = p.CATEGORY_fk');
  $balance->join('supplier', 's', 's.ID = p.SUPPLIER_fk');
  $balance->join('meeting', 'm', 'm.ID = p.MEETING_fk');
  $balance->join('payments_type', 'pt', 'pt.ID = p.PAYMENT_TYPE_fk');
  $balance->join('category', 'ct', 'ct.ID = p.CATEGORY_fk');
  $fields['balance'] = $balance->fields('p')
    ->fields('c', array('name'))
    ->fields('s', array('company_name', 'first_name', 'last_name'))
    ->fields('m', array('meeting_name','date'))
    ->fields('pt', array( 'payments_name',))
    ->where('p.UID_fk = :uid', array(':uid' => $user->uid))
    ->execute()
    ->fetchAllAssoc('ID', PDO::FETCH_ASSOC);

  return theme('user_profile_theme', array('form' => $form, 'paypal' => $paypal, 'balance' => $fields['balance']));
}

/**
 * Profile update
 */

function user_profile_edit($form, &$form_state) {
  global $user;
  $form['last_name'] = array(
    '#type' => 'hidden',
  );
  $form['first_name'] = array(
    '#type' => 'hidden',
  );
  $form['address'] = array(
    '#type' => 'hidden',
  );
  $form['country'] = array(
    '#type' => 'hidden',
  );
  $form['city'] = array(
    '#type' => 'hidden',
  );
  $form['email'] = array(
    '#type' => 'hidden',
  );
  $form['phone'] = array(
    '#type' => 'hidden',
  );
  $form['web_address'] = array(
    '#type' => 'hidden',
  );
  $form['twitter'] = array(
    '#type' => 'hidden',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#attributes' => array('class' => array('hidden',)),
  );
  return $form;
}

function user_profile_edit_submit($form, &$form_state) {
  global $user;
  $data = array(
    'company' => '',
    'first_name' => '',
    'last_name' => '',
    'address' => '',
    'country' => '',
    'city' => '',
    'email' => '',
    'phone' => '',
    'web_address' => '',
    'twitter' => '',
  );
  $account = user_load($user->uid);
  foreach ($form_state['values'] as $key => $val) {
    if ($val != '' && isset($data[$key])) {
      $data[$key] = $val;
    }
  }
  $changes = array();
  foreach ($data as $key => $val) {
    if ($val != '') {
      $changes[$key] = $val;
    }
  }

  $save_data = user_save($account, array('data' => $changes));

}

function _user_payment($form, $form_state) {

  $payment = new Payment(array(
    'context_data' => array( // Add whatever you want in this array, this is passed to the payment method. If your are using Paypal Payment method, these informations are also POSTed to Paypal
      //'nid' => $nid,
    ),
    'currency_code' => 'USD',
    'description' => t('Product description'),
    'finish_callback' => '_mycheckoutmodule_payment_complete', // The function providing the page where the user is redirected after the payment
  ));
  $payment->setLineItem(new PaymentLineItem(array(
    'amount' => 20,
    'name' => t('Product name'),
    'description' => t('Product description'),
    'tax_rate' => '0.2',
    'quantity' => 1,
  )));
  $form = payment_form_standalone($form, $form_state, $payment);

  return $form;
}


