<style>
  .form-item {
    margin: 0;
  !important;
  }
</style>
<?php
$inline = <<<IN
jQuery(document).ready(function(){
var $ = jQuery;
$('.editable~td button.edit').each(function(i,e){

  $(e).click(function(){

    ele = $(this).parent().siblings().children('span');

    text = ele.text();
    data_id = ele.parent().data('id');
    data_name = ele.parent().data('name');

    input = $('<input>').val(text);
    ele.hide();
    ele.after(input);
    input.focus();

    input.blur(function() {
      ele.text($(this).val());
      var input_field = $('input[name="' +data_id+'"]');
      input_field.val($(this).val());
      input_field.siblings('input[name="id"]').val(data_name);
      input_field.siblings('input[name="type"]').val('update');
      input_field.parent().parent().submit();
      $(this).remove();
      ele.show();
    });
  });
});

$('.editable~td button.delete').each(function(i,e){

  $(e).click(function(){

    ele = $(this).parent().siblings();
    data_id = ele.data('id');
    data_name = ele.data('name');

    var input_field = $('input[name="' +data_id+'"]');
    input_field.val($(this).val());
    input_field.siblings('input[name="id"]').val(data_name);
    input_field.siblings('input[name="type"]').val('delete');
    input_field.parent().parent().submit();

  });
});
});

IN;

drupal_add_js($inline, array(
  'type' => 'inline',
  'group' => JS_LIBRARY,
  'weight' => -100
));

?>
<!-- hidden -->
<?php echo drupal_render($meeting_type_edit) ?>
<?php echo drupal_render($location_edit) ?>
<?php echo drupal_render($category_edit) ?>
<?php echo drupal_render($payment_edit) ?>

<!--Filter Bar-->
<div class="container-fluid">
  <div class="row">
    <div class="container">
      <div class="col-lg-12 exp-company-profile " style="margin-top:20px; margin-bottom:20px;">
        <div class="row">
          <div class="col-lg-6">
            <h3 class="txt-cyan" style="margin-bottom:0px; line-height:1.7em;"> Add New/Edit </h3>
          </div>
          <div class="col-lg-3 col-lg-offset-3 hidden">
            <table class="table table-bordered filter-bar" style="margin-bottom:0px;">
              <tr>
                <td style=" line-height:2em;"> Company Names :</td>
                <td>
                  <button type="button" class="btn btn-default dropdown-toggle btn-xs btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                      class="fa fa-bars"></i> <span class="caret"></span></button>
                  <ul class="dropdown-menu dropdown-menu-right company-list">
                    <li><a href="#">AbcFood-Williams(Will)</a></li>
                    <li><a href="#">Nestle-Robbin (Rob)</a></li>
                    <li><a href="#">Volvic-Cameron (Cam)</a></li>
                  </ul>
                </td>
                <!--<td>
                                	<button class="btn btn-xs btn-success"><i class="fa fa-edit"></i> Edit</button>
                                </td>-->
              </tr>
            </table>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Table Calander-->
<div class="container-fluid exp-main-container">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <div class="exp-tables padding-top0">
            <header>

              <div class="col-lg-7">
                <h4><span class="exp-title-1 txt-blue">Add/Edit Meeting Type</span></h4>
              </div>
              <div class="col-lg-5 text-right">

              </div>


              <div class="clearfix"></div>
            </header>
            <table class="table table-bordered dataTable">
              <thead>
              <tr class=" bg-blue bg-gray">
                <th>Meeting Type</th>
                <th>Edit/Delete</th>
              </tr>
              </thead>
              <thead>
              <tr>
                <form action="<?php echo $meeting_type['#action']; ?>" method="<?php echo $meeting_type['#method']; ?>" id="<?php echo $meeting_type['#id']; ?>" accept-charset="">
                  <th><?php echo drupal_render($meeting_type['meeting_name']); ?></th>

                  <th><?php
                    echo drupal_render($meeting_type['submit']);
                    ?></th>
                  <?php
                  $form_key = element_children($meeting_type);
                  foreach ($form_key as $key) {
                    if ($key != 'meeting_name' AND $key != 'submit') {
                      echo drupal_render($meeting_type[$key]);
                    }
                  }
                  ?>
                </form>
              </tr>
              </thead>
              <tbody>

              <?php
              foreach ($meeting_type_list as $meeting_type) {
                echo '<tr>';
                echo "<td class='editable' data-name='{$meeting_type['ID']}' data-id='meeting_name_edit'><span> {$meeting_type['name']}</span></td>";
                echo '<td>
                          <button type="button" class="btn btn-xs btn-info edit"><i class="fa fa-edit"></i></button>
                          <button type="button" class="btn btn-xs btn-danger delete"><i class="fa fa-trash-o"></i></button>
                        </td>';
                echo '</tr>';
              }
              ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="exp-tables padding-top0">
            <header>
              <div class="col-lg-7">
                <h4><span class="exp-title-1 txt-blue">Add/Edit Meeting Location</span></h4>
              </div>
              <div class="col-lg-5 text-right">

              </div>
              <div class="clearfix"></div>
            </header>
            <table class="table table-bordered dataTable">
              <thead>
              <tr class=" bg-blue bg-gray">
                <th>Meeting Location</th>
                <th>Add/Edit/Delete</th>

              </tr>

              </thead>
              <thead>
              <tr>
                <form action="<?php echo $location['#action']; ?>" method="<?php echo $location['#method']; ?>" id="<?php echo $location['#id']; ?>" accept-charset="">
                  <th><?php echo drupal_render($location['location_name']); ?></th>

                  <th><?php
                    echo drupal_render($location['submit']);
                    ?></th>
                  <?php
                  $form_key = element_children($location);

                  foreach ($form_key as $key) {
                    if ($key != 'location_name' AND $key != 'submit') {
                      echo drupal_render($location[$key]);
                    }
                  }
                  ?>
                </form>
              </tr>
              </thead>
              <tbody>

              <?php
              foreach ($location_list as $meeting_type) {
                echo '<tr>';
                echo "<td class='editable' data-name='{$meeting_type['ID']}' data-id='location_name_edit'><span> {$meeting_type['name']}</span></td>";
                echo '<td>
                          <button type="button" class="btn btn-xs btn-info edit"><i class="fa fa-edit"></i></button>
                          <button type="button" class="btn btn-xs btn-danger delete"><i class="fa fa-trash-o"></i></button>
                        </td>';
                echo '</tr>';
              }
              ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="clearfix"></div>

      </div>

      <div class="row" style="border-top:1px solid #d7d7d7; padding-top:30px; margin-top:30px;">
        <div class="col-lg-6">
          <div class="exp-tables padding-top0">
            <header>

              <div class="col-lg-7">
                <h4><span class="exp-title-1 txt-blue">Add/Edit Catagory</span></h4>
              </div>
              <div class="col-lg-5 text-right">

              </div>


              <div class="clearfix"></div>
            </header>
            <table class="table table-bordered dataTable">
              <thead>
              <tr class=" bg-blue bg-gray">
                <th>Catagory Type</th>
                <th>Edit/Delete</th>

              </tr>
              </thead>
              <thead>
              <tr>
                <form action="<?php echo $category['#action']; ?>" method="<?php echo $category['#method']; ?>" id="<?php echo $category['#id']; ?>" accept-charset="">
                  <th><?php echo drupal_render($category['category_name']); ?></th>

                  <th><?php
                    echo drupal_render($category['submit']);
                    ?></th>
                  <?php
                  $form_key = element_children($category);

                  foreach ($form_key as $key) {
                    if ($key != 'category_name' AND $key != 'submit') {
                      echo drupal_render($category[$key]);
                    }
                  }
                  ?>
                </form>
              </tr>
              </thead>
              <tbody>

              <?php
              foreach ($category_list as $meeting_type) {
                echo '<tr>';
                echo "<td class='editable' data-name='{$meeting_type['ID']}' data-id='category_name_edit'><span> {$meeting_type['name']}</span></td>";
                echo '<td>
                          <button type="button" class="btn btn-xs btn-info edit"><i class="fa fa-edit"></i></button>
                          <button type="button" class="btn btn-xs btn-danger delete"><i class="fa fa-trash-o"></i></button>
                        </td>';
                echo '</tr>';
              }
              ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="exp-tables padding-top0">
            <header>
              <div class="col-lg-7">
                <h4><span class="exp-title-1 txt-blue">Add/Edit Payment </span></h4>
              </div>
              <div class="col-lg-5 text-right">

              </div>
              <div class="clearfix"></div>
            </header>
            <table class="table table-bordered dataTable">
              <thead>
              <tr class=" bg-blue bg-gray">
                <th>Payment Type</th>
                <th>Add/Edit/Delete</th>

              </tr>

              </thead>
              <thead>
              <tr>
                <form action="<?php echo $payment['#action']; ?>" method="<?php echo $payment['#method']; ?>" id="<?php echo $payment['#id']; ?>" accept-charset="">
                  <th><?php echo drupal_render($payment['payment_name']); ?></th>

                  <th><?php
                    echo drupal_render($payment['submit']);
                    ?></th>
                  <?php
                  $form_key = element_children($payment);

                  foreach ($form_key as $key) {
                    if ($key != 'payment_name' AND $key != 'submit') {
                      echo drupal_render($payment[$key]);
                    }
                  }
                  ?>
                </form>
              </tr>
              </thead>
              <tbody>

              <?php
              foreach ($payment_list as $meeting_type) {
                echo '<tr>';
                echo "<td class='editable' data-name='{$meeting_type['ID']}' data-id='payment_name_edit'> <span> {$meeting_type['payments_name']}</span></td>";
                echo '<td>
                          <button type="button" class="btn btn-xs btn-info edit"><i class="fa fa-edit"></i></button>
                          <button type="button" class="btn btn-xs btn-danger delete"><i class="fa fa-trash-o"></i></button>
                        </td>';
                echo '</tr>';
              }
              ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="clearfix"></div>

      </div>
    </div>
  </div>
</div>

<!-- Event Calander-->
<div class="container-fluid exp-main-container hidden" style="background-color: transparent;">
  <div class="row">
    <div class="container">
      <div class="col-lg-8 null-padding">
        <div class="text-center">
          <h1 class="exp-title-1">Events</h1>
        </div>
        <ul>
          <li class="media">
            <div class="media-left event-date"><a href="#"> 16 <span>Sep</span> </a></div>
            <div class="media-body">
              <h4 class="media-heading txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 10:20PM<span>
              <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra
                turpis. Fusce condimentum nunc ac nisi vulputate fringilla. </p>
            </div>
          </li>
          <li class="media">
            <div class="media-left event-date"><a href="#"> 16 <span>Sep</span> </a></div>
            <div class="media-body">
              <h4 class="media-heading txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 01:00PM<span>
              <p> Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit. </p>
            </div>
          </li>
          <li class="media">
            <div class="media-left event-date"><a href="#"> 16 <span>Sep</span> </a></div>
            <div class="media-body">
              <h4 class="media-heading  txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 4:30PM<span>
              <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra
                turpis. Fusce condimentum nunc ac nisi vulputate fringilla. </p>
            </div>
          </li>
        </ul>
      </div>
      <div class="col-lg-4 null-padding">
        <div class="custom-calendar-wrap">
          <div id="custom-inner" class="custom-inner">
            <div class="custom-header clearfix">
              <nav><span id="custom-prev" class="custom-prev"></span> <span id="custom-next" class="custom-next"></span></nav>
              <h2 id="custom-month" class="custom-month"></h2>

              <h3 id="custom-year" class="custom-year"></h3>
            </div>
            <div id="calendar" class="fc-calendar-container"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>