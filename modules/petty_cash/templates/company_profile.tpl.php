<?php
$inline = <<<IN
jQuery(document).ready(function(){
var $ = jQuery;
$('.editable i').each(function(i,e){

  $(e).click(function(){
    ele = $(this).siblings();
    text = ele.text();
    data = ele.parent().data('id');

    input = $('<input>').val(text);
    ele.hide();
    ele.after(input);
    input.focus();

    input.blur(function() {
      ele.text($(this).val());
      $('input[name="' +data+'"]').val($(this).val());
      $(this).remove();
      ele.show();
    });
  });
});
});
IN;

/*drupal_add_css('//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css', array('type' => 'external', 'group' => CSS_DEFAULT));*/
drupal_add_js($inline, array(
  'type' => 'inline',
  'group' => JS_LIBRARY,
  'weight' => -100
));

?>
<?php echo drupal_render($form); ?>
<?php
$total_balance = 0;
$currency_sign = '$';
$items = array();

foreach ($balance as $val) {
  $total_balance += $val['amount'];
}
foreach ($balance as $val) {
  if (!isset($items[$val['CATEGORY_fk']])) {
    $items[$val['CATEGORY_fk']] = array(
      'name' => $val['name'],
      'amount' => $val['amount'],
      'delivered_amount' => ($val['is_delivered'] ? $val['amount'] : 0),
      'qty' => $val['qty'],
      'balance' => ($val['amount'] - ($val['is_delivered'] ? $val['amount'] : 0))
    );
  }
  else {
    $items[$val['CATEGORY_fk']]['amount'] += $val['amount'];
    $items[$val['CATEGORY_fk']]['delivered_amount'] += ($val['is_delivered'] ? $val['amount'] : 0);
    $items[$val['CATEGORY_fk']]['qty'] += $val['qty'];
    $items[$val['CATEGORY_fk']]['balance'] += ($val['amount'] - ($val['is_delivered'] ? $val['amount'] : 0));
  }
}
?>
<!--Filter Bar-->
<div class="container-fluid">
  <div class="row">
    <div class="container">
      <div class="col-lg-12 exp-company-profile " style="margin-top:20px; margin-bottom:-20px;">
        <div class="row">
          <div class="col-lg-6">
            <h3 class="txt-cyan" style="margin-bottom:0px; line-height:1.7em; font-family: 'OpenSans-Semibold'"><i class="fa fa-building"></i> Company Profile</h3>
          </div>
          <div class="col-lg-3 col-lg-offset-3 hidden">
            <table class="table table-bordered filter-bar" style="margin-bottom:0px;">
              <tr>
                <td style=" line-height:2em;"> Supplier Names :</td>
                <td>
                  <button type="button" class="btn btn-default dropdown-toggle btn-xs btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                      class="fa fa-bars"></i> <span class="caret"></span></button>
                  <ul class="dropdown-menu dropdown-menu-right company-list">
                    <li><a href="#">AbcFood-Williams(Will)</a></li>
                    <li><a href="#">Nestle-Robbin (Rob)</a></li>
                    <li><a href="#">Volvic-Cameron (Cam)</a></li>
                  </ul>
                </td>
                <!--<td>
                  <button class="btn btn-xs btn-success"><i class="fa fa-edit"></i> Edit</button>
               </td>-->
              </tr>
            </table>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Welcome Container-->
<div class="container-fluid exp-main-container border-top0" style="background-color:transparent;">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 ">
          <div class=" exp-company-profile" id='company-profile'>
            <h3 class="txt-blue"> Profile</h3>
            <table class="table table-bordered">
              <tr>
                <td>Comany Name</td>
                <td class="editable" data-id="company_name"><span><?php echo $user->data['company'] ?></span><i
                    class="pull-right btn btn-xs btn-success glyphicon glyphicon-pencil"></i></td>
              </tr>
              <tr>
                <td>Contact First Name</td>
                <td class="editable" data-id="first_name"><span><?php echo $user->data['first_name'] ?></span> <i
                    class="pull-right btn btn-xs btn-success glyphicon glyphicon-pencil"></i></td>
              </tr>
              <tr>
                <td>Contact Last Name</td>
                <td class="editable" data-id="last_name"><span><?php echo $user->data['last_name'] ?></span> <i
                    class="pull-right btn btn-xs btn-success glyphicon glyphicon-pencil"></i></td>
              </tr>
              <tr>
                <td>Address</td>
                <td class="editable" data-id="address"><span><?php echo $user->data['address'] ?></span> <i
                    class="pull-right btn btn-xs btn-success glyphicon glyphicon-pencil"></i></td>
              </tr>
              <tr>
                <td>City</td>
                <td class="editable" data-id="city"><span><?php echo $user->data['city'] ?></span> <i class="pull-right btn btn-xs btn-success glyphicon glyphicon-pencil"></i></td>
              </tr>
              <tr>
                <td>Country</td>
                <td class="editable" data-id="country"><span><?php echo $user->data['country'] ?></span> <i
                    class="pull-right btn btn-xs btn-success glyphicon glyphicon-pencil"></i></td>
              </tr>
              <tr>
                <td>Tel No.</td>
                <td class="editable" data-id="phone"><span><?php echo $user->data['phone'] ?></span> <i class="pull-right btn btn-xs btn-success glyphicon glyphicon-pencil"></i>
                </td>
              </tr>
              <tr>
                <td>Website Address</td>
                <td class="editable" data-id="web_address"><span><?php echo $user->data['phone'] ?></span> <i
                    class="pull-right btn btn-xs btn-success glyphicon glyphicon-pencil"></i></td>
              </tr>
              <tr>
                <td>Twitter ID</td>
                <td class="editable" data-id="twitter"><span><?php echo $user->data['twitter'] ?></span> <i
                    class="pull-right btn btn-xs btn-success glyphicon glyphicon-pencil"></i>
                </td>
              </tr>
              <tr>
                <td>Account Type</td>
                <td>Standard</td>
              </tr>
            </table>

            <div class="text-right">
              <button class="btn btn-xs btn-info" onclick="jQuery('#user-profile-edit').submit()"><i class="fa fa-edit"></i> Edit</button>
            </div>

          </div>


        </div>


        <div class="col-lg-6">
          <div class="exp-company-profile" id='company-expense'>
            <div>
              <div class="col-lg-6 null-padding">
                <h3 class="txt-cyan"> Company Balance</h3>
              </div>

              <div class="col-lg-6 null-padding text-right" style="padding-top:10px;"><span
                  class="label label-danger txt-big"><?php echo $total_balance < 0 ? '-' . $currency_sign . abs($total_balance) : $currency_sign . $total_balance ?></span></div>
              <div class="clearfix"></div>
            </div>
            <table class="table table-responsive table-bordered exp-table">
              <thead>
              <tr class="bg-blue bg-gray">
                <th>Products</th>
                <th>Paid</th>
                <th>Delivery Value</th>
                <th>Balance</th>
              </tr>
              </thead>
              <tbody>
              <?php
              $total_balance = 0;
              foreach ($items as $val) {
                $total_balance += $val['balance'];
                echo '<tr>';
                echo '<td>' . $val['name'] . '</td>';
                echo '<td>' . $val['amount'] . '</td>';
                echo '<td>' . $val['delivered_amount'] . '</td>';
                echo '<td>' . $val['balance'] . '</td>';
                echo '</tr>';
              }
              ?>
              </tbody>
              <tfoot>
              <tr>
                <td colspan="2"><span class="txt-big">Total Balance</span></td>
                <td></td>
                <td><span class="txt-big"><b><?php echo $total_balance < 0 ? '-' . $currency_sign . abs($total_balance) : $currency_sign . $total_balance ?></b></span></td>
              </tr>
              </tfoot>
            </table>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>

<!-- Table Calander-->
<div class="container-fluid exp-main-container">
  <div class="row">
    <div class="container">
      <div class="exp-tables">
        <header>
          <div class="col-lg-7">
            <h3><i class="fa fa-truck"></i> <span class="exp-title-1 txt-blue">Received Delivery Value </span></h3>
          </div>
          <div class="col-lg-5 text-right">
            <!--<button class="btn btn-default btn-xs btn-success"><i class="fa fa-plus"></i> Add</button>
                        <button class="btn btn-default btn-xs btn-success"><i class="fa fa-edit"></i> Edit</button>-->
          </div>
          <div class="clearfix"></div>
        </header>
        <table class="table table-bordered dataTable">
          <thead>
          <tr class=" bg-blue bg-gray">
            <th>Date</th>
            <th>Company Name</th>
            <th>Contact FnLname</th>
            <th>Meeting</th>
            <th>Catogry</th>
            <th>Pay Type</th>
            <th>Desc</th>
            <th>Amount</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>01-Sep-2015</td>
            <td>XYZ Pvt ltd</td>
            <td>Williams(Will)</td>
            <td>T2Project</td>
            <td>Snacks</td>
            <td>Cash</td>
            <td>Kam</td>
            <td>$1000</td>
          </tr>
          <tr>
            <td>10-Sep-2015</td>
            <td>XYZ Pvt ltd</td>
            <td>Williams(Will)</td>
            <td>T2Project</td>
            <td>Water</td>
            <td>Cash</td>
            <td>Kam</td>
            <td>$2000</td>
          </tr>
          <tr>
            <td>11-Sep-2015</td>
            <td>Nestle</td>
            <td>Robin(Rob)</td>
            <td>T2Project</td>
            <td>Snacks</td>
            <td>Cheque</td>
            <td>HSBC</td>
            <td>$1250</td>
          </tr>
          </tbody>
        </table>
      </div>
      <div class="exp-tables">
        <header>
          <div class="col-lg-7">
            <h3><i class="fa fa-money"></i> <span class="exp-title-1 txt-blue">Amount Paid in Advance</span></h3>
          </div>
          <div class="col-lg-5 text-right">
            <!--<button class="btn btn-default btn-xs btn-success"><i class="fa fa-plus"></i> Add</button>
                        <button class="btn btn-default btn-xs btn-success"><i class="fa fa-edit"></i> Edit</button>-->
          </div>
          <div class="clearfix"></div>
        </header>

        <table class="table  table-bordered dataTable">
          <thead>
          <tr class=" bg-blue bg-gray">
            <th>Date</th>
            <th>Company Name</th>
            <th>Contact FnLname</th>
            <th>Meeting</th>
            <th>Catogry</th>
            <th>Pay Type</th>
            <th>Desc</th>
            <th>Amount</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>01-Sep-2015</td>
            <td>XYZ Pvt ltd</td>
            <td>Williams(Will)</td>
            <td>T2Project</td>
            <td>Snacks</td>
            <td>Cash</td>
            <td>Kam</td>
            <td>$300</td>
          </tr>
          <tr>
            <td>10-Sep-2015</td>
            <td>XYZ Pvt ltd</td>
            <td>Williams(Will)</td>
            <td>T2Project</td>
            <td>Water</td>
            <td>Cash</td>
            <td>Kam</td>
            <td>$300</td>
          </tr>
          <tr>
            <td>11-Sep-2015</td>
            <td>Nestle</td>
            <td>Robin(Rob)</td>
            <td>T2Project</td>
            <td>Snacks</td>
            <td>Cheque</td>
            <td>HSBC</td>
            <td>$200</td>
          </tr>
          </tbody>
        </table>
      </div>
      <div class="exp-tables">
        <header>
          <div class="col-lg-7">
            <h3><i class="fa fa-users"></i> <span class="exp-title-1 txt-blue">UpComming Meetings</span></h3>
          </div>
          <div class="col-lg-5 text-right">
            <!--<button class="btn btn-default btn-xs btn-success"><i class="fa fa-plus"></i> Add</button>
                        <button class="btn btn-default btn-xs btn-success"><i class="fa fa-edit"></i> Edit</button>-->
          </div>
          <div class="clearfix"></div>
        </header>

        <table class="table table-bordered dataTable">
          <thead>
          <tr class=" bg-blue bg-gray">
            <th>Date</th>
            <th>Company Name</th>
            <th>Contact FnLname</th>
            <th>Meeting</th>
            <th>Catogry</th>
            <th>Location</th>
            <th>Amount</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>01-Sep-2015</td>
            <td>XYZ Pvt ltd</td>
            <td>Williams(Will)</td>
            <td>T2Project</td>
            <td>Snacks</td>
            <td>HIltonCD</td>
            <td>$300</td>
          </tr>
          <tr>
            <td>10-Sep-2015</td>
            <td>XYZ Pvt ltd</td>
            <td>Williams(Will)</td>
            <td>T2Project</td>
            <td>Water</td>
            <td>MariotBC</td>
            <td>$300</td>
          </tr>
          <tr>
            <td>11-Sep-2015</td>
            <td>Nestle</td>
            <td>Robin(Rob)</td>
            <td>T2Project</td>
            <td>Snacks</td>
            <td>HIltonCD</td>
            <td>$200</td>
          </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Event Calander-->
<div class="container-fluid exp-main-container  hidden" style="background-color: transparent;">
  <div class="row">
    <div class="container">
      <div class="col-lg-8 event-section">
        <div class="text-center">
          <h1 class="exp-title-1">Events</h1>
        </div>
        <ul>
          <li class="media">
            <div class="media-left event-date"><a href="#"> 16 <span>Sep</span> </a></div>
            <div class="media-body">
              <h4 class="media-heading txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 10:20PM<span>
              <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra
                turpis. Fusce condimentum nunc ac nisi vulputate fringilla. </p>
            </div>
          </li>
          <li class="media">
            <div class="media-left event-date"><a href="#"> 16 <span>Sep</span> </a></div>
            <div class="media-body">
              <h4 class="media-heading txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 01:00PM<span>
              <p> Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit. </p>
            </div>
          </li>
          <li class="media">
            <div class="media-left event-date"><a href="#"> 16 <span>Sep</span> </a></div>
            <div class="media-body">
              <h4 class="media-heading  txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 4:30PM<span>
              <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra
                turpis. Fusce condimentum nunc ac nisi vulputate fringilla. </p>
            </div>
          </li>
        </ul>
      </div>
      <div class="col-lg-4">
        <div class="custom-calendar-wrap">
          <div id="custom-inner" class="custom-inner">
            <div class="custom-header clearfix">
              <nav><span id="custom-prev" class="custom-prev"></span> <span id="custom-next" class="custom-next"></span></nav>
              <h2 id="custom-month" class="custom-month"></h2>

              <h3 id="custom-year" class="custom-year"></h3>
            </div>
            <div id="calendar" class="fc-calendar-container"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
