<!-- Fix of .div-table .div-cell -->
<style type="text/css">
  .form-item, .form-actions {
    margin-top: 1em;
    margin-bottom: 1em;
    display: table-cell;
    width: 6%;
  }

  .form-type-checkbox {
    width: 3%;
  !important;
  }

  .supplier_table div.form-type-textfield {
    display: table-cell;
    width: 6%;
    padding: 5px;
    border-left: 1px solid #d7d7d7;
    border-bottom: 0px;
    border-top: 0px;
  }

</style>


<!--Filter Bar-->
<div class="container-fluid">
  <div class="row">
    <div class="container">
      <div class="col-lg-12 exp-company-profile " style="margin-top:20px; margin-bottom:-20px;">
        <div class="row">
          <div class="col-lg-6">
            <h3 class="txt-cyan" style="margin-bottom:0px; line-height:1.7em;">Add Meeting</h3>
          </div>
          <div class="col-lg-3 col-lg-offset-3 hidden">
            <table class="table table-bordered filter-bar" style="margin-bottom:0px;">
              <tr>
                <td style=" line-height:2em;"> Company Names :</td>
                <td>
                  <button type="button" class="btn btn-default dropdown-toggle btn-xs btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                      class="fa fa-bars"></i> <span class="caret"></span></button>
                  <ul class="dropdown-menu dropdown-menu-right company-list">
                    <li><a href="#">AbcFood-Williams(Will)</a></li>
                    <li><a href="#">Nestle-Robbin (Rob)</a></li>
                    <li><a href="#">Volvic-Cameron (Cam)</a></li>
                  </ul>
                </td>

              </tr>
            </table>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Main Container-->
<div class="container-fluid exp-main-container border-top0" style="background-color:transparent; min-height:72.3vh;">
  <form class="" action="<?php echo $form['#action']; ?>" method="<?php echo $form['#method']; ?>" id="<?php echo $form['#id']; ?>" accept-charset="">
    <div class="container">

      <div class="row" style="margin-bottom:30px;">
        <div class="col-lg-12" style="margin-bottom:5px;"><h4> Meeting</h4></div>
        <div class="col-lg-12 div-table">
          <div class="div-row bg-blue bg-gray">
            <div class="div-cell">Meeting Name</div>
            <div class="div-cell">Meeting Type</div>
            <div class="div-cell">Meeting Location</div>
            <div class="div-cell">Meeting Date</div>

          </div>
          <div class="div-row bg-white">
            <?php
            $form_key = element_children($form);
            $meeting_key = array(
              'name',
              'type',
              'location',
              'date'
            );
            foreach ($form_key as $key) {
              if (in_array($key, $meeting_key)) {
                echo drupal_render($form[$key]);
              }
            }
            ?>
          </div>

        </div>


      </div>

      <div class="row">
        <div class="col-lg-12" style="margin-bottom:5px;"><h4> Suppliers</h4></div>
        <div class="col-lg-12 div-table">
          <div class="div-row bg-blue bg-gray">
            <div class="div-cell" style="width:3%; text-align:center;"> #</div>
            <div class="div-cell"> Company Name</div>
            <div class="div-cell">Contact F-Name</div>
            <div class="div-cell">Contact L-Name</div>
            <div class="div-cell">Address</div>
            <div class="div-cell"> Country</div>
            <div class="div-cell"> City</div>
            <div class="div-cell"> Email</div>
            <div class="div-cell"> Website</div>
            <div class="div-cell"> Twitter ID</div>
            <div class="div-cell"> Tel. NO.</div>
            <div class="div-cell"> Save/ Edit</div>
          </div>
          <div class="div-row bg-white">
            <?php
            $meeting_key[] = 'supplier';
            foreach ($form['supplier'] as $key => $val) {
              if (is_numeric($key)) {
                echo '<div class="supplier_table">';
                echo drupal_render($val);
                echo '</div>';
              }
            }
            ?>

            <div class="div-cell">
              <!--<button class="btn btn-xs btn-success" type="button" id="add_clone_field"><i class="fa fa-plus"></i> Add</button>-->
            </div>
          </div>
        </div>
      </div>

      <div class="row" style="margin-top:20px;">
        <div class="col-lg-12 text-right">
          <?php
          foreach ($form_key as $key) {
            if (!in_array($key, $meeting_key)) {
              echo drupal_render($form[$key]);
            }
          }
          ?>
          <!--<button type="button" class="btn btn-xs btn-success"><i class="fa fa-save"></i> Save</button>-->
        </div>
      </div>
  </form>

</div>

<?php
drupal_add_js("
var clone_div;
  jQuery(document).ready(function(){
    clone_div = jQuery(jQuery('.supplier_table')[0]).clone();
  });
  jQuery('#add_clone_field').on('click', function(e){
        jQuery(e.currentTarget).parent().parent().prepend(clone_div);
    });
 ", array(
  'group' => JS_LIBRARY,
  'type' => 'inline',
));

