
<!--Filter Bar-->
<div class="container-fluid">
  <div class="row">
    <div class="container">
      <div class="col-lg-12 exp-company-profile " style="margin-top:20px; margin-bottom:20px;">
        <div class="row">
          <div class="col-lg-6">
            <h3 class="txt-cyan" style="margin-bottom:0px; line-height:1.7em; font-family: 'OpenSans-Semibold'"><i class="fa fa-thumbs-up"></i> Upcoming Meetings</h3>
          </div>
          <div class="col-lg-3 col-lg-offset-3 hidden">
            <table class="table table-bordered filter-bar" style="margin-bottom:0px;">
              <tr>
                <td style=" line-height:2em;" > Company Names : </td>
                <td><button type="button" class="btn btn-default dropdown-toggle btn-xs btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                  <ul class="dropdown-menu dropdown-menu-right company-list">
                    <li><a href="#" >AbcFood-Williams(Will)</a></li>
                    <li><a href="#">Nestle-Robbin (Rob)</a></li>
                    <li><a href="#">Volvic-Cameron (Cam)</a></li>
                  </ul></td>
                <!--<td>
                                	<button class="btn btn-xs btn-success"><i class="fa fa-edit"></i> Edit</button>
                                </td>--> 
              </tr>
            </table>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Table Calander-->
<div class="container-fluid exp-main-container" >
  <div class="row">
    <div class="container">
      <div class="exp-tables">
        <header>
          <!--<div class="col-lg-7">
            <h3><i class="fa fa-truck"></i> <span class="exp-title-1 txt-blue">Received Delivery Value </span></h3>
          </div>
          <div class="col-lg-5 text-right"> 
            <!--<button class="btn btn-default btn-xs btn-success"><i class="fa fa-plus"></i> Add</button>
                        <button class="btn btn-default btn-xs btn-success"><i class="fa fa-edit"></i> Edit</button>
          </div>
          <div class="clearfix"></div>
        </header>--> 
        <table class="table table-bordered dataTable">
          <thead>
            <tr class=" bg-blue bg-gray">
              <th>Date</th>
              <th>Meeting</th>
              <th>Location</th>
              
              <th>Type</th>
            </tr>
          </thead>
          <tbody>

              <?php
              foreach ($meetings as $val) {
                echo '<tr>';
                echo '<td>' . date('Y-m-d', strtotime($val['date'])) . '</td>';
                echo '<td>' . $val['meeting_name'] . '</td>';
                echo '<td>' . $val['l_name'] . '</td>';
                echo '<td>' . $val['name'] . '</td>';
                echo '</tr>';
              }
              ?>

          </tbody>
        </table>
      </div>
      
      
    </div>
  </div>
</div>
