<?php
/**
 * Created by PhpStorm.
 * User: hitesh
 * Date: 23-9-15
 * Time: 17:22
 */
//var_dump($form);

?>


<!--Filter Bar-->
<div class="container-fluid">
  <div class="row">
    <div class="container">
      <div class="col-lg-12 exp-company-profile " style="margin-top:20px; margin-bottom:-20px;">
        <div class="row">
          <div class="col-lg-6">
            <h3 class="txt-cyan" style="margin-bottom:0px; line-height:1.7em;">Add seller</h3>
          </div>
          <div class="col-lg-3 col-lg-offset-3 hidden">
            <table class="table table-bordered filter-bar" style="margin-bottom:0px;">
              <tr>
                <td style=" line-height:2em;"> Company Names :</td>
                <td>
                  <button type="button" class="btn btn-default dropdown-toggle btn-xs btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                      class="fa fa-bars"></i> <span class="caret"></span></button>
                  <ul class="dropdown-menu dropdown-menu-right company-list">
                    <li><a href="#">AbcFood-Williams(Will)</a></li>
                    <li><a href="#">Nestle-Robbin (Rob)</a></li>
                    <li><a href="#">Volvic-Cameron (Cam)</a></li>
                  </ul>
                </td>

              </tr>
            </table>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Main Container-->
<div class="container-fluid exp-main-container border-top0" style="background-color:transparent;">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 ">
          <div class=" exp-company-profile">
            <h3 class="txt-blue"> Profile</h3>

            <!-- Form Supplier Start -->
            <form class="form-horizontal" action="<?php echo $form['#action']; ?>"  method="<?php echo $form['#method']; ?>" id="<?php echo $form['#id']; ?>" accept-charset="">
              <?php
              $form_key = element_children($form);
              foreach ($form_key as $key) {
                if (is_array($form[$key])) {
                  echo drupal_render($form[$key]);
                }
              }
              ?>
            </form>
            <!--- ---->
          </div>
        </div>
        <div class="col-lg-6">
          <div class="exp-company-profile"
               style="height:68.5vh ; padding:5px;;">
            <img src="/<?php echo drupal_get_path('theme',
              'ExpenseManager'); ?>/images/seller.jpg" alt="seller"
                 style="width:100% ; height:100%;"/>

            <div class="clearfix"></div>
          </div>

        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>