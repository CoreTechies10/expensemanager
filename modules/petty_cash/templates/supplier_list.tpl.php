<!--Filter Bar-->
<div class="container-fluid">
  <div class="row">
    <div class="container">
      <div class="col-lg-12 exp-company-profile " style="margin-top:20px; margin-bottom:-20px;">
        <div class="row">
          <div class="col-lg-6">
            <h3 class="txt-cyan" style="margin-bottom:0px; line-height:1.7em;">Add/ Edit Amount</h3>
          </div>
          <div class="col-lg-3 col-lg-offset-3 hidden">
            <table class="table table-bordered filter-bar" style="margin-bottom:0px;">
              <tr>
                <td style=" line-height:2em;"> Company Names :</td>
                <td>
                  <button type="button" class="btn btn-default dropdown-toggle btn-xs btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                      class="fa fa-bars"></i> <span class="caret"></span></button>
                  <ul class="dropdown-menu dropdown-menu-right company-list">
                    <li><a href="#">AbcFood-Williams(Will)</a></li>
                    <li><a href="#">Nestle-Robbin (Rob)</a></li>
                    <li><a href="#">Volvic-Cameron (Cam)</a></li>
                  </ul>
                </td>
                <!--<td>
                        <button class="btn btn-xs btn-success"><i class="fa fa-edit"></i> Edit</button>
                    </td>-->
              </tr>
            </table>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Welcome Container-->
<div class="container-fluid exp-main-container border-top0" style="background-color:transparent; min-height:465px;">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 ">
          <div class=" exp-company-profile">
            <!--<h3 class="txt-blue"> Profile</h3>-->
            <table class="table table-bordered dataTable">
              <thead>
              <tr class="bg-blue bg-gray">
                <th>Company Name</th>
                <th>Supplier Name</th>
                <th></th>
              </tr>
              </thead>
              <tbody>
                <?php
                foreach ($profile as $key => $val) {
                  echo '<tr>';
                    echo '<td>'.$val['company_name'].'</td>';
                    echo '<td>'.$val['first_name'].'</td>';
                    echo '<td><a href="/expense_manager/supplier/'. $val['ID'].'">Details</a> </td>';
                  echo '</tr>';
                }
                ?>

              </tbody>
            </table>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>
