<?php
$inline = <<<IN
jQuery(document).ready(function(){
var $ = jQuery;
$('.editable span').each(function(i,e){

  $(e).click(function(){
    ele = $(this);
    text = $(this).text();

    data_id = ele.parent().data('id');
    data_type = ele.parent().data('type');
    data_field = ele.parent().data('field');
    data_option = ele.parent().data('option');

    ele.hide();


    input = $('<input>').val(text);
    select = $('<select>');


    if(data_type == 'select'){
       select.append(data_option);
       ele.after(select);

       select.focus();
       select.blur(function() {

         ele.text($(this).find(':selected').text());

         var form = $('#form-supplier-edit');
         form.find('input[name="id"]').val(data_id);
         form.find('input[name="type"]').val(data_field);
         form.find('input[name="operation"]').val('update');
         form.find('input[name="value"]').val($(this).val());
         form.submit();
         $(this).remove();
         ele.show();
       });

    }else{
      ele.after(input);
      input.focus();
      input.blur(function() {
        ele.text($(this).val());

        var form = $('#form-supplier-edit');
        form.find('input[name="id"]').val(data_id);
        form.find('input[name="type"]').val(data_field);
        form.find('input[name="operation"]').val('update');
        form.find('input[name="value"]').val($(this).val());

        form.submit();
        $(this).remove();
        ele.show();
      });
    }

  });
});
$('.editable_profile i').each(function(i,e){

  $(e).click(function(){
    ele = $(this).siblings();
    text = ele.text();
    data = ele.parent().data('id');
    form_id = ele.parent().data('form_id');
    value_id = ele.parent().data('value_id');

    input = $('<input>').val(text);
    ele.hide();
    ele.after(input);
    input.focus();

    input.blur(function() {
      ele.text($(this).val());
      $('#'+form_id+' [name="' +data+'"]').val($(this).val());
      $('#'+form_id+' input[name="id"]').val(value_id);
      $(this).remove();
      ele.show();
    });
  });
});
});
IN;

/*drupal_add_css('//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css', array('type' => 'external', 'group' => CSS_DEFAULT));*/
drupal_add_js($inline, array(
  'type' => 'inline',
  'group' => JS_LIBRARY,
  'weight' => -100
));
?>

<?php echo drupal_render($edit_form); ?>
<?php echo drupal_render($update_form); ?>

<!-- Form Supplier Start
  <form class="form-horizontal" action="<?php echo $form['#action']; ?>" method="<?php echo $form['#method']; ?>" id="<?php echo $form['#id']; ?>" accept-charset="">
  <?php
    $form_key = element_children($form);
    foreach ($form_key as $key) {
      if (is_array($form[$key])) {
        echo drupal_render($form[$key]);
      }
    }
  ?>
  </form>
---->

<!--Filter Bar-->
<div class="container-fluid">
  <div class="row">
    <div class="container">
      <div class="col-lg-12 exp-company-profile " style="margin-top:20px; margin-bottom:-20px;">
        <div class="row">
          <div class="col-lg-6">
            <h3 class="txt-cyan" style="margin-bottom:0px; line-height:1.7em; font-family: 'OpenSans-Semibold'"><i class="fa fa-truck"></i> <?php echo $profile['company_name'] ?>
            </h3>
          </div>
          <div class="col-lg-3 col-lg-offset-3 hidden">
            <table class="table table-bordered filter-bar" style="margin-bottom:0px;">
              <tr>
                <td style=" line-height:2em;"> Supplier Names :</td>
                <td>
                  <button type="button" class="btn btn-default dropdown-toggle btn-xs btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                      class="fa fa-bars"></i> <span class="caret"></span></button>
                  <ul class="dropdown-menu dropdown-menu-right company-list">
                    <li><a href="#">AbcFood-Williams(Will)</a></li>
                    <li><a href="#">Nestle-Robbin (Rob)</a></li>
                    <li><a href="#">Volvic-Cameron (Cam)</a></li>
                  </ul>
                </td>

              </tr>
            </table>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Main Container-->
<div class="container-fluid exp-main-container border-top0" style="background-color:transparent;">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 ">
          <div class=" exp-company-profile" id='company-profile'>
            <h3 class="txt-blue"> Profile</h3>
            <table class="table table-bordered">

              <tr>
                <td>Comany Name</td>
                <td class="editable_profile" data-id="company_name" data-form_id="form-update-supplier" data-value_id="<?php echo $profile['ID'] ?>"><span><?php echo $profile['company_name'] ?></span><i
                    class="pull-right btn btn-xs btn-success glyphicon glyphicon-pencil"></i></td>
              </tr>
              <tr>
                <td>Contact First Name</td>
                <td class="editable_profile" data-id="first_name" data-form_id="form-update-supplier"  data-value_id="<?php echo $profile['ID'] ?>"><span><?php echo $profile['first_name'] ?></span> <i
                    class="pull-right btn btn-xs btn-success glyphicon glyphicon-pencil"></i></td>
              </tr>
              <tr>
                <td>Contact Last Name</td>
                <td class="editable_profile" data-id="last_name" data-form_id="form-update-supplier"  data-value_id="<?php echo $profile['ID'] ?>"><span><?php echo $profile['last_name'] ?></span> <i
                    class="pull-right btn btn-xs btn-success glyphicon glyphicon-pencil"></i></td>
              </tr>
              <tr>
                <td>Address</td>
                <td class="editable_profile" data-id="address" data-form_id="form-update-supplier"  data-value_id="<?php echo $profile['ID'] ?>"><span><?php echo $profile['address'] ?></span> <i
                    class="pull-right btn btn-xs btn-success glyphicon glyphicon-pencil"></i></td>
              </tr>
              <tr>
                <td>City</td>
                <td class="editable_profile" data-id="city" data-form_id="form-update-supplier"  data-value_id="<?php echo $profile['ID'] ?>"><span><?php echo $profile['city'] ?></span> <i class="pull-right btn btn-xs btn-success glyphicon glyphicon-pencil"></i></td>
              </tr>
              <tr>
                <td>Country</td>
                <td class="editable_profile" data-id="country" data-form_id="form-update-supplier"  data-value_id="<?php echo $profile['ID'] ?>"><span><?php echo $profile['country'] ?></span> <i
                    class="pull-right btn btn-xs btn-success glyphicon glyphicon-pencil"></i></td>
              </tr>
              <tr>
                <td>Tel No.</td>
                <td class="editable_profile" data-id="phone" data-form_id="form-update-supplier"  data-value_id="<?php echo $profile['ID'] ?>"><span><?php echo $profile['phone'] ?></span> <i class="pull-right btn btn-xs btn-success glyphicon glyphicon-pencil"></i>
                </td>
              </tr>
              <tr>
                <td>Website Address</td>
                <td class="editable_profile" data-id="web_address" data-form_id="form-update-supplier"  data-value_id="<?php echo $profile['ID'] ?>"><span><?php echo $profile['phone'] ?></span> <i
                    class="pull-right btn btn-xs btn-success glyphicon glyphicon-pencil"></i></td>
              </tr>
              <tr>
                <td>Twitter ID</td>
                <td class="editable_profile" data-id="twitter" data-form_id="form-update-supplier"  data-value_id="<?php echo $profile['ID'] ?>"><span><?php echo $profile['twitter'] ?></span> <i
                    class="pull-right btn btn-xs btn-success glyphicon glyphicon-pencil"></i>
                </td>
              </tr>

            </table>

            <div class="text-right">
              <button class="btn btn-xs btn-info" onclick="jQuery('#form-update-supplier').submit()"><i class="fa fa-edit"></i> Save</button>
            </div>
          </div>
        </div>

        <!-- Company Profile Money  -->

        <?php
        $total_balance = 0;
        $currency_sign = '$';
        $items = array();

        foreach ($balance as $val) {
          $total_balance += $val['amount'];
        }
        foreach ($balance as $val) {
          if (!isset($items[$val['CATEGORY_fk']])) {
            $items[$val['CATEGORY_fk']] = array(
              'name' => $val['name'],
              'amount' => $val['amount'],
              'delivered_amount' => ($val['is_delivered'] ? $val['amount'] : 0),
              'qty' => $val['qty'],
              'balance' => ($val['amount'] - ($val['is_delivered'] ? $val['amount'] : 0))
            );
          }
          else {
            $items[$val['CATEGORY_fk']]['amount'] += $val['amount'];
            $items[$val['CATEGORY_fk']]['delivered_amount'] += ($val['is_delivered'] ? $val['amount'] : 0);
            $items[$val['CATEGORY_fk']]['qty'] += $val['qty'];
            $items[$val['CATEGORY_fk']]['balance'] += ($val['amount'] - ($val['is_delivered'] ? $val['amount'] : 0));
          }
        }
        ?>

        <div class="col-lg-6">
          <div class="exp-company-profile" id='company-expense'>
            <div>
              <div class="col-lg-6 null-padding">
                <h3 class="txt-cyan"> Company Balance</h3>
              </div>

              <div class="col-lg-6 null-padding text-right" style="padding-top:10px;"><span
                  class="label label-danger txt-big"><?php echo $total_balance < 0 ? '-' . $currency_sign . abs($total_balance) : $currency_sign . $total_balance ?></span></div>
              <div class="clearfix"></div>
            </div>
            <table class="table table-responsive table-bordered exp-table">
              <thead>
              <tr class="bg-blue bg-gray">
                <th>Products</th>
                <th>Paid</th>
                <th>Delivery Value</th>
                <th>Balance</th>
              </tr>
              </thead>
              <tbody>
              <?php
              $total_balance = 0;
              foreach ($items as $val) {
                $total_balance += $val['balance'];
                echo '<tr>';
                echo '<td>' . $val['name'] . '</td>';
                echo '<td>' . $val['amount'] . '</td>';
                echo '<td>' . $val['delivered_amount'] . '</td>';
                echo '<td>' . $val['balance'] . '</td>';
                echo '</tr>';
              }
              ?>
              </tbody>
              <tfoot>
              <tr>
                <td colspan="2"><span class="txt-big">Total Balance</span></td>
                <td></td>
                <td><span class="txt-big"><b><?php echo $total_balance < 0 ? '-' . $currency_sign . abs($total_balance) : $currency_sign . $total_balance ?></b></span></td>
              </tr>
              </tfoot>
            </table>
          </div>
        </div>


        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>

<!-- Table-->
<div class="container-fluid exp-main-container">
  <div class="row">
    <div class="container">
      <div class="exp-tables">
        <header>
          <div class="col-lg-7">
            <h3><i class="fa fa-truck"></i> <span class="exp-title-1 txt-blue"> Delivery Value from <?php echo $profile['company_name'] ?> </span></h3>
          </div>
          <div class="col-lg-5 text-right exp-delivery hidden">
            <button type="button" class="btn btn-xs btn-success exp-addRow-btn"><i class="fa fa-plus"></i> Add Row</button>
            <button type="button" class="btn btn-xs btn-success exp-addRow-btn"><i class="fa fa-plus"></i> Add Column</button>
            <button type="button" class="btn btn-xs btn-info exp-edit-btn"><i class="fa fa-edit"></i> Edit</button>
            <button type="button" class="btn btn-xs btn-primary exp-editinline-btn"><i class="fa fa-edit"></i> Edit</button>
            <button type="button" class="btn btn-xs btn-danger  exp-delete-btn"><i class="fa fa-trash-o"></i> Delete</button>
            <button type="button" class="btn btn-xs btn-default exp-cancel-btn"><i class="fa fa-trash-o"></i> Cancel</button>

          </div>
          <div class="clearfix"></div>
        </header>
        <table class="table dataTable table-bordered exp-data-table" id="delivery-table">
          <thead>
          <tr class="bg-blue bg-gray">
            <th>Date</th>
            <th>Company Name</th>
            <th>Contact FnLname</th>
            <th>Meeting</th>
            <th>Category</th>
            <th>Pay Type</th>
            <th>Amount</th>
          </tr>
          </thead>
          <tbody>
          <?php
          foreach ($balance as $val) {
            $val['is_delivered'] = ($val['is_delivered'] == 'false' || $val['is_delivered'] == '') ? FALSE : TRUE;
            if ($val['is_delivered']) {
              echo '<tr>';
              echo "<td class='' data-type='date' data-id='{$val['ID']}' data-field='delivery_date'><span>" . date('Y-m-d', strtotime($val['date'])) . "</span></td>";
              echo "<td>{$val['first_name']}</td>";
              echo "<td>{$val['last_name']}</td>";
              echo "<td class='editable' data-type='select' data-option='" . array_to_options($meetings, 'ID', 'meeting_name') . "' data-id='{$val['ID']}' data-field='delivery_meeting'><span>{$val['meeting_name']}</span></td>";
              echo "<td class='editable' data-type='select' data-option='" . array_to_options($categories, 'ID', 'name') . "' data-id='{$val['ID']}' data-field='delivery_category'><span>{$val['name']}</span></td>";
              echo "<td class='editable' data-type='select' data-option='" . array_to_options($payment_types, 'ID', 'payments_name') . "' data-id='{$val['ID']}' data-field='delivery_payment'><span>{$val['payments_name']}</span></td>";
              echo "<td class='editable' data-type='number' data-id='{$val['ID']}' data-field='delivery_amount'><span>{$val['amount']}</span></td>";
              echo '</tr>';
            }
          }
          ?>
          </tbody>
        </table>
      </div>


      <div class="exp-tables">
        <header>
          <div class="col-lg-7">
            <h3><i class="fa fa-money"></i> <span class="exp-title-1 txt-blue">Amount Paid in Advance to <?php echo $profile['company_name'] ?></span></h3>
          </div>
          <div class="col-lg-5 text-right exp-amount hidden">
            <button type="button" class="btn btn-xs btn-success exp-addRow-btn"><i class="fa fa-plus"></i> Add Row</button>
            <button type="button" class="btn btn-xs btn-success exp-addRow-btn"><i class="fa fa-plus"></i> Add Column</button>
            <button type="button" class="btn btn-xs btn-info exp-edit-btn"><i class="fa fa-edit"></i> Edit</button>
            <button type="button" class="btn btn-xs btn-primary exp-editinline-btn"><i class="fa fa-edit"></i> Edit</button>
            <button type="button" class="btn btn-xs btn-danger  exp-delete-btn"><i class="fa fa-trash-o"></i> Delete</button>
            <button type="button" class="btn btn-xs btn-default exp-cancel-btn"><i class="fa fa-trash-o"></i> Cancel</button>
          </div>

          <div class="clearfix"></div>
        </header>
        <table class="table dataTable table-bordered">
          <thead>
          <tr class="bg-blue bg-gray">
            <th>Date</th>
            <th>Company Name</th>
            <th>Contact FnLname</th>
            <th>Meeting</th>
            <th>Category</th>
            <th>Pay Type</th>
            <th>Amount</th>
          </tr>
          </thead>
          <tbody>
          <?php
          foreach ($balance as $val) {
            $val['is_advance'] = ($val['is_advance'] == 'false' || $val['is_advance'] == '') ? FALSE : TRUE;
            if ($val['is_advance']) {
              echo '<tr>';
              echo "<td class='' data-type='date' data-id='{$val['ID']}' data-field='delivery_date'><span>" . date('Y-m-d', strtotime($val['date'])) . "</span></td>";
              echo "<td>{$val['first_name']}</td>";
              echo "<td>{$val['last_name']}</td>";
              echo "<td class='editable' data-type='select' data-option='" . array_to_options($meetings, 'ID', 'meeting_name') . "' data-id='{$val['ID']}' data-field='delivery_meeting'><span>{$val['meeting_name']}</span></td>";
              echo "<td class='editable' data-type='select' data-option='" . array_to_options($categories, 'ID', 'name') . "' data-id='{$val['ID']}' data-field='delivery_category'><span>{$val['name']}</span></td>";
              echo "<td class='editable' data-type='select' data-option='" . array_to_options($payment_types, 'ID', 'payments_name') . "' data-id='{$val['ID']}' data-field='delivery_payment'><span>{$val['payments_name']}</span></td>";
              echo "<td class='editable' data-type='number' data-id='{$val['ID']}' data-field='delivery_amount'><span>{$val['amount']}</span></td>";
              echo '</tr>';
            }
          }
          ?>
          </tbody>
        </table>
      </div>


      <div class="exp-tables">
        <header>
          <div class="col-lg-7">
            <h3><i class="fa fa-users"></i> <span class="exp-title-1 txt-blue">UpComming Meetings</span></h3>
          </div>
          <div class="col-lg-5 text-right exp-upcomming hidden">
            <button type="button" class="btn btn-xs btn-success exp-addRow-btn"><i class="fa fa-plus"></i> Add Row</button>
            <button type="button" class="btn btn-xs btn-success exp-addRow-btn"><i class="fa fa-plus"></i> Add Column</button>
            <button type="button" class="btn btn-xs btn-info exp-edit-btn"><i class="fa fa-edit"></i> Edit</button>
            <button type="button" class="btn btn-xs btn-primary exp-editinline-btn"><i class="fa fa-edit"></i> Edit</button>
            <button type="button" class="btn btn-xs btn-danger  exp-delete-btn"><i class="fa fa-trash-o"></i> Delete</button>
            <button type="button" class="btn btn-xs btn-default exp-cancel-btn"><i class="fa fa-trash-o"></i> Cancel</button>
          </div>
          <div class="clearfix"></div>
        </header>
        <table class="table dataTable table-bordered">
          <thead>
          <tr class="bg-blue bg-gray">
            <th>Date</th>
            <th>Company Name</th>
            <th>Contact FnLname</th>
            <th>Meeting</th>
            <th>Category</th>
            <th>Location</th>
            <th>Amount</th>
          </tr>
          </thead>
          <tbody>
          <?php
          foreach ($balance as $val) {
            $opening_date = new DateTime($val['date']);
            $current_date = new DateTime();

            if ($opening_date > $current_date) {
              echo '<tr>';
              echo '<td>' . date('Y-m-d', strtotime($val['date'])) . '</td>';
              echo "<td>{$val['first_name']}</td>";
              echo "<td>{$val['last_name']}</td>";
              echo "<td>{$val['meeting_name']}</td>";
              echo "<td>{$val['name']}</td>";
              echo "<td>{$val['payments_name']}</td>";
              echo "<td>{$val['amount']}</td>";
              echo '</tr>';
            }
          }
          ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Event Calander-->
<div class="container-fluid exp-main-container hidden" style="background-color: transparent;">
  <div class="row">
    <div class="container">
      <div class="col-lg-8 event-section">
        <div class="text-center">
          <h1 class="exp-title-1">Events</h1>
        </div>
        <ul>
          <li class="media">
            <div class="media-left event-date"><a href="#"> 16 <span>Sep</span> </a></div>
            <div class="media-body">
              <h4 class="media-heading txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 10:20PM<span>
              <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra
                turpis. Fusce condimentum nunc ac nisi vulputate fringilla. </p>
            </div>
          </li>
          <li class="media">
            <div class="media-left event-date"><a href="#"> 16 <span>Sep</span> </a></div>
            <div class="media-body">
              <h4 class="media-heading txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 01:00PM<span>
              <p> Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit. </p>
            </div>
          </li>
          <li class="media">
            <div class="media-left event-date"><a href="#"> 16 <span>Sep</span> </a></div>
            <div class="media-body">
              <h4 class="media-heading  txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 4:30PM<span>
              <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra
                turpis. Fusce condimentum nunc ac nisi vulputate fringilla. </p>
            </div>
          </li>
        </ul>
      </div>
      <div class="col-lg-4">
        <div class="custom-calendar-wrap">
          <div id="custom-inner" class="custom-inner">
            <div class="custom-header clearfix">
              <nav><span id="custom-prev" class="custom-prev"></span> <span id="custom-next" class="custom-next"></span></nav>
              <h2 id="custom-month" class="custom-month"></h2>

              <h3 id="custom-year" class="custom-year"></h3>
            </div>
            <div id="calendar" class="fc-calendar-container"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
