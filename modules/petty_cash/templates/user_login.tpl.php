
<!--Filter Bar-->
<div class="container-fluid">
  <div class="row">
    <div class="container">
      <div class="col-lg-12 exp-company-profile " style="margin-top:20px; margin-bottom:-20px;">
        <div class="row">
          <div class="col-lg-6">
            <h3 class="txt-cyan" style="margin-bottom:0px; line-height:1.7em;">Login</h3>
          </div>
          <div class="col-lg-3 col-lg-offset-3">
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Main Container-->
<div class="container-fluid exp-main-container border-top0" style="background-color:transparent; min-height:72.6vh;">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 ">
          <div class=" exp-company-profile" >
            <form class="form-horizontal" action="<?php echo $form['#action']; ?>" method="<?php echo $form['#method']; ?>" id="<?php echo $form['#id']; ?>" accept-charset="">
              <?php
              $form_key = element_children($form);
              foreach ($form_key as $key) {
                if (is_array($form[$key])) {
                  echo drupal_render($form[$key]);
                }
              }
              ?>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="exp-company-profile"  style="height:355px ; padding:5px;;">
            <img src="/<?php echo drupal_get_path('theme','ExpenseManager') ?>/images/1.jpg" alt="seller" style="width:100% ; height:100%;"/>
            <div class="clearfix"></div>
          </div>

        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>
