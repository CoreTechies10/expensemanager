<?php
/**
 * Created by PhpStorm.
 * User: hitesh
 * Date: 18-9-15
 * Time: 13:12
 */


/**
 * Overriding defalut login system
 */
/*function ExpenseManager_themes(&$items){

  $items = array();
  //Changing Default Login system
  $items['user_login'] = array(
    'render element' => 'form',
    'path' =>drupal_get_path('theme','ExpenseManager') . '/templates',
    'templates' => 'user-login',
    'preprocess function' => array(
      'ExpenseManager_preprocess_user_login',
    )
  );
  //Changing Default register system
  $items['uesr_register_form'] = array();

  //Changing Default passsword
  // @Todo need proper comment (Don't know what is this doing Ref:- https://www.drupal.org/node/350634)
  $items['uesr_pass'] = array();

}*/

/*function ExpenseManager_theme(){
    return array(
    'user_login' => array(
      'template' => 'user-login',
      'variables' => array('form' => NULL), ## you may remove this line in this case
      'path' => drupal_get_path('theme', 'ExpenseManager') . '/templates',
      'preprocess function' => array(
          'ExpenseManager_preprocess_user_login',
        )
    ),
    'user_register' => array(
      'template' => 'user-register',
      //'variables' => array('form' => NULL), ## you may remove this line in this case
      'path' => drupal_get_path('theme', 'ExpenseManager') . '/templates'
    ),
    'user_pass' => array(
      'template' => 'user-pass',
      //'variables' => array('form' => NULL), ## you may remove this line in this case
      'path' => drupal_get_path('theme', 'ExpenseManager') . '/templates'
    ),
  );
}
*/

function ExpenseManager_preprocess(&$variables){
  
  global $user;
  if (!$user->uid) {
    $form = drupal_get_form('user_login');
    
    //user name field
    $form['name']['#attributes'] = array(
      'class' => array('form-control'),
      'placeholder' => 'Company Name',
      'required' => 'required',
    );
    $form['name']['#description'] = '';
    $form['name']['#title_display' ] = 'hidden';
    
    $form['name']['#field_prefix'] = '<li>';
    $form['name']['#field_suffix'] = '</li>';
    $form['name']['#title_prefix_suffix'] = FALSE;
    

    //user password field
    $form['pass']['#attributes'] = array(
      'class' => array('form-control'),
      'placeholder' => 'Password',
      'required' => 'required',
    );
    $form['pass']['#description'] = '';
    $form['name']['#title_display' ] = 'hidden';
    $form['name']['#field_prefix'] = '<li>';
    $form['name']['#field_suffix'] = '</li>';
    $form['pass']['#title_prefix_suffix'] = FALSE;


    //user submit button
    $form['actions']['submit']['#attributes'] = array('class' => array('btn', 'btn-xs', 'btn-success'));
    $form['actions']['submit']['#prefix'] = ' ';
    $form['actions']['submit']['#suffix'] = '';
    
    $variables['user_login_form'] = $form;
  }
}


function ExpenseManager_form_element($variables){

 $element = &$variables['element'];

  // This function is invoked as theme wrapper, but the rendered form element
  // may not necessarily have been processed by form_builder().
  $element += array(
    '#title_display' => 'before',
  );

  // Add element #id for #type 'item'.
  if (isset($element['#markup']) && !empty($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  // Add element's #type and #name as class to aid with JS/CSS selectors.
  $attributes['class'] = array('form-item');
  
  if (!empty($element['#type'])) {
    $attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');
  }
  if (!empty($element['#name'])) {
    $attributes['class'][] = 'form-item-' . strtr($element['#name'], array(' ' => '-', '_' => '-', '[' => '-', ']' => ''));
  }
  // Add a class for disabled elements to facilitate cross-browser styling.
  if (!empty($element['#attributes']['disabled'])) {
    $attributes['class'][] = 'form-disabled';
  }

  $output = '<div' . drupal_attributes($attributes) . '>' . "\n";

  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }
  
  //@Todo add array for div class
  $prefix = isset($element['#field_prefix']) ?  $element['#field_prefix']  : '';
  $suffix = isset($element['#field_suffix']) ? $element['#field_suffix'] : '';

  switch ($element['#title_display']) {
    case 'before':
        $output .= ' ' . theme('form_element_label', $variables);
        $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
        break;
    case 'invisible':
        $output .= ' ' . theme('form_element_label', $variables);
        $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;
     case 'hidden':
        $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;
    case 'after':
      $output .= ' ' . $prefix . $element['#children'] . $suffix;
      $output .= ' ' . theme('form_element_label', $variables) . "\n";
      break;

    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;
  }

  if (!empty($element['#description'])) {
    $output .= '<div class="description">' . $element['#description'] . "</div>\n";
  }

  $output .= "</div>\n";

  return $output;
}

function ExpenseManager_form_element_label($variables){
 $element = $variables['element'];
  // This is also used in the installer, pre-database setup.
  $t = get_t();

  // If title and required marker are both empty, output no label.
  if ((!isset($element['#title']) || $element['#title'] === '') && empty($element['#required'])) {
    return '';
  }

  // If the element is required, a required marker is appended to the label.
  $required = !empty($element['#required']) ? theme('form_required_marker', array('element' => $element)) : '';

  $title = filter_xss_admin($element['#title']);

  $attributes = array();
  // Style the label as class option to display inline with the element.
  if ($element['#title_display'] == 'after') {
    $attributes['class'][] = 'option';
  }
  // Show label only to screen readers to avoid disruption in visual flows.
  elseif ($element['#title_display'] == 'invisible') {
    $attributes['class'][] = 'element-invisible';
    $attributes['class'][] = 'hidden'; # updated
  }
  
  #updated
  if(isset($element['#title_attributes'])){
    $attributes['class'][] = $element['#title_attributes'];
  }

  
  
  if (!empty($element['#id'])) {
    $attributes['for'] = $element['#id'];
  }

  $prefix = isset($element['#title_prefix']) ? $element['#title_prefix'] : '';
  $suffix = isset($element['#title_suffix']) ? $element['#title_suffix'] : '';
  
  /******************/

  if(isset($element['#title_prefix_suffix'])){
    return $prefix . ' <label' . drupal_attributes($attributes) . '>' . $t('!title !required', array('!title' => $title, '!required' => $required)) . "</label>\n" . $suffix . "\n";
  }
  
  // The leading whitespace helps visually separate fields from inline labels.
  return ' <label' . drupal_attributes($attributes) . '>' . $t('!title !required', array('!title' => $title, '!required' => $required)) . "</label>\n";
}