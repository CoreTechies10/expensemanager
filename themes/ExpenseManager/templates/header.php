﻿<header class="container-fluid exp-header">
  <div class="row">
    <div class="container">
      <div class="col-lg-3"> <span class="exp-logo">Expense<span class="txt-cyan"> Manager</span></span> </div>
      <div class="col-lg-9 exp-menu">
        <ul class="cl-effect-1">
        <?php if($logged_in):?>
          
          <li class="dropdown"> <a href="#" id="meetings" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Supplier</a>
            <ul class="dropdown-menu dropdown-menu-right exp-dropdown-menu" aria-labelledby="meetings">
              <li><a href="/expense_manager/supplier/">Lists Supplier</a></li>
              <li><a href="/expense_manager/supplier/create">Create Supplier</a></li>
            </ul>
          </li>
          
          <li class="dropdown"> <a href="#" id="meetings" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Meetings</a>
            <ul class="dropdown-menu dropdown-menu-right exp-dropdown-menu" aria-labelledby="meetings">
              <li><a href="/expense_manager/meeting/">Lists Meetings</a></li>
              <li><a href="/expense_manager/meeting/create">Create Meetings</a></li>
            </ul>
          </li>
          
          
          <li><a href="/expense_manager/add_edit">Catagories</a></li>
          
          <li><a href="#">Reports</a></li>
          <!--<li class="dropdown"> <a href="#" id="search" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-search"></i></a>
            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="search" style="width:250px;">
              <form>
                <li>
                  <div class="col-lg-12" style="padding:0px;">
                    <div class="input-group"> <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                      </span>
                      <input type="text" class="form-control" placeholder="Search for...">
                    </div>
                    <!-- /input-group /->
                  </div>
                  <!-- /.col-lg-6 /->
                </li>
              </form>
            </ul> 
          </li>-->
          <?php endif; ?>
          
          <?php if($is_admin): ?>
            <li class="dropdown">
                  <a href="/admin" id="login" >Admin Panel</a>
           </li>
          <?php endif; ?>
          
          <?php if($logged_in): ?>
                <!-- @Todo Region Login  -->
               <li class="dropdown">
                <a href="#" id="login" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $user->name; ?></a> <!-- Logged in user's name will come here -->
            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="login">
              
                <li class="text-center">
                    <a href="/user" style="font-size:14px !important;">My Profile</a><!-- Logged in user's profile link will goes here -->
                </li>
                <li class="divider"></li>
                <li class="text-right" id="exp-link">
                  <span  class="btn btn-md btn-info btn-block"> <a href="/user/logout"> Logout</a></span>
                </li>
            </ul>
          </li>
          <?php else: ?>
               <li > <a href="/user/register" type="button" ><i class="fa fa-user"></i> Register</a>
             <li class="dropdown"> <a href="#" id="login" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> Login</a>
            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="login">
              <form class="form-horizontal" action="<?php echo $user_login_form['#action']; ?>" method="<?php echo $user_login_form['#method']; ?>" id="<?php echo $user_login_form['#id']; ?>" accept-charset="">
              <?php
              $form_key = element_children($user_login_form);
              foreach ($form_key as $key) {
                if (is_array($user_login_form[$key])) {
                  echo drupal_render($user_login_form[$key]);
                }
              }
              ?>
                  <div class="clearfix"></div>
                </form>
            </ul>
          </li>
          <?php endif; ?>
          
          
        </ul>
      </div>
    </div>
  </div>
</header>