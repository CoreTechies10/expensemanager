<!-- <?php echo __FILE__; ?> -->
<body >
<?php include_once 'header.php'; ?>
    
    
<!-- Slider -->
<div class="container-fluid exp-slider">
  <div class="row">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        <li data-target="#carousel-example-generic" data-slide-to="3"></li>
      </ol>
      
      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <div class="item active"> <img src="<?php echo drupal_get_path('theme', 'ExpenseManager'); ?>/images/banner.jpg" alt="...">
          <div class="carousel-caption"> </div>
        </div>
        <div class="item"> <img src="<?php echo drupal_get_path('theme', 'ExpenseManager'); ?>/images/banner1.jpg" alt="...">
          <div class="carousel-caption"> </div>
        </div>
        <div class="item"> <img src="<?php echo drupal_get_path('theme', 'ExpenseManager'); ?>/images/banner.jpg" alt="...">
          <div class="carousel-caption"> </div>
        </div>
        <div class="item"> <img src="<?php echo drupal_get_path('theme', 'ExpenseManager'); ?>/images/banner1.jpg" alt="...">
          <div class="carousel-caption"> </div>
        </div>
      </div>
      
      <!-- Controls -->
      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
  </div>
</div>

<!-- Welcome Container-->
<div class="container-fluid exp-main-container">
  <div class="row">
    <div class="container">
      <h1 class="text-capitalize text-center">Welcome</h1>
      <p>Lorem ipsum dolor sit amet consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean onummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis di arturient ontes nascetur ridiculus mus. Nulla du.</p>
    </div>
  </div>
</div>

<!-- Service Container-->
<div class="container-fluid exp-main-container" style="background-color:transparent;">
  <div class="row">
    <div class="container">
      <h1 class="text-capitalize text-center">Our Services</h1>
      <div class="exp-service-containe">
        <div class="col-lg-6">
          <figure class="exp-image"> <img src="<?php echo drupal_get_path('theme', 'ExpenseManager'); ?>/images/11.jpg" alt="premium"> </figure>
          <section class="exp-section">
            <div class="col-lg-6">
              <h4 class="txt-blue">About Premium Service</h4>
              <p>Lorem ipsum dolor sit amet consectetuer adipiscing elit. Praesent vetie lacus. Aenean ummy heerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus magnis diturient ontes nascetur ridiculula. Ut enim ad minima veniam.</p>
            </div>
            <div class="col-lg-6">
              <h4 class="txt-blue">Premium Services</h4>
              <ul>
                <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                <li><a href="#">Lorem ipsum dolor sit amet</a></li>
              </ul>
            </div>
            <div class="clearfix"></div>
          </section>
        </div>
        <div class="col-lg-6">
          <figure class="exp-image"> <img src="<?php echo drupal_get_path('theme', 'ExpenseManager'); ?>/images/10.png" alt="standars"> </figure>
          <section class="exp-section">
            <div class="col-lg-6">
              <h4 class="txt-blue">About Standard Service</h4>
              <p>Lorem ipsum dolor sit amet consectetuer adipiscing elit. Praesent vetie lacus. Aenean ummy heerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus magnis diturient ontes nascetur ridiculula. Ut enim ad minima veniam.</p>
            </div>
            <div class="col-lg-6">
              <h4 class="txt-blue">Standard Services</h4>
              <ul>
                <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                <li><a href="#">Lorem ipsum dolor sit amet</a></li>
              </ul>
            </div>
            <div class="clearfix"></div>
          </section>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>


<footer class="container-fluid exp-footer ">
  <div class="row">
    <div class="container">
    	<div class="col-lg-6">
        	<span>Provided by</span> <b>CoreTechies</b>
        </div>
        <div class="col-lg-6">
        	<ul>
            	<li><a href="#">Settings</a></li>
                <li><a href="#"><i class="fa fa-question-circle"></i></a></li>
            </ul>
        </div>
    </div>
  </div>
</footer>

</body>

