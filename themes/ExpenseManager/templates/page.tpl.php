<!-- <?php echo __FILE__;  ?> -->
<body >

<?php include_once 'header.php'; ?>


<?php if($show_messages): ?>
<!--Alert Bar-->
<div class="container-fluid">
  <div class="row">
    <div class="container">
      <div class="col-lg-12 exp-company-profile " style="margin-top:20px; margin-bottom:20px;">
        <?php echo  $messages ?>
      </div>
    </div>
  </div>
</div>
<?php endif;?>

<?php echo drupal_render($page['content']); ?>


<footer class="container-fluid exp-footer ">
  <div class="row">
    <div class="container">
    	<div class="col-lg-6">
        	<span>Provided by</span> <b>CoreTechies</b>
        </div>
        <div class="col-lg-6">
        	<ul>
            	<li><a href="#">Settings</a></li>
                <li><a href="#"><i class="fa fa-question-circle"></i></a></li>
            </ul>
        </div>
    </div>
  </div>
</footer>

</body>

